import pandas as pd
studenq= pd.read_csv('../Questionnaire 20-21.csv', delimiter=';',decimal=',')

# Q.1.a. What is the probability that a randomly selected student had 2 hours or less of mathematics in the final year of secondary school?
print(studenq.shape[0])
# U = 21
# G = 6
q1a = (6/21)
print(q1a)


# Q.1.b. What is the probability that a randomly selected student had 3 hours or more of mathematics in the final year of secondary school?
# U = 21
# G = 15
q1b = (15/21)
print(q1b)

# Q.2 What is the probability that a randomly selected student is a non-smoker or eats more than 3 pieces of fruit?
# U = 21
# nonSmoker = studenq.apply(lambda x: True if x['Smoker'] == 'No' else False , axis=1)
# numOfRows = len(nonSmoker[nonSmoker == True].index)
# # print(numOfRows)
# # G = 16
# # G = 0
# q2 = (16/21)
# print(q2)

a=len(studenq.loc[studenq.Smoker=='No'])
b=len(studenq.loc[(studenq.Fruit=='3') | (studenq.Fruit=='4') | (studenq.Fruit=='5 or more')])
c=len(studenq.loc[(studenq.Smoker=='No') &((studenq.Fruit=='3') | (studenq.Fruit=='4') | (studenq.Fruit=='5 or more'))])
print("sol:")
print((a+b-c)/len(studenq.Fruit))

# Q.3 What is the probability of a randomly selected smoking student eating 1 or less pieces of fruit?
# Universe = 4
# Gsmoke = 3
# q3 = Gsmoke / Universe
# print(q3)

d=len(studenq.loc[(studenq.Smoker=='Yes') & (studenq.Fruit=='1 or less')])
print("sol")
print(d/len(studenq.loc[studenq.Smoker=='Yes']))

# Q.4 Are the events 'having a driver licence' and ‘right writing hand' independent?
Students = 21
RightHandedStudents = 21
# Because this is one, it is independant
# With this being 1 nothing changes
DriversLicenseStudents = 6
# INDEPENDANT

dl = len(studenq.loc[studenq['Driver License'] == 'Yes'])
rh = len(studenq.loc[studenq['Writing Hand'] == 'Right'])
dl_rh = len(studenq.loc[(studenq['Driver License'] == 'Yes') & (studenq['Writing Hand'] == 'Right')])
total = len(studenq)
print("sol:")
print(dl_rh / total-(dl / total) * (rh / total))


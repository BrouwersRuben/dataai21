import pandas as pd
from scipy.stats import binom
from scipy.stats import norm
studenq= pd.read_csv('../Questionnaire 20-21.csv', delimiter=';',decimal=',')

# Q.1 What is the probability that of 10 randomly selected students, exactly 3 know their blood group?
bloodtypeknown = len(studenq.loc[(studenq['Blood Type'] != 'Unknown') & (studenq['Resus Factor'] != 'Unknown')])
total = len(studenq)
print("sol")
print(binom.pmf(3,10,bloodtypeknown/total))

# Q.2.A What is the probability that a student underestimates his studies (i.e. that 1 credit corresponds to 15 or fewer hours)?
underestimate = len(studenq.loc[(studenq['Study Load'] == '<= 10 hours') | (studenq['Study Load'] == '11-15 hours')])
total = len(studenq)
print("sol")
print(underestimate/total)

# Q.2.B You have to make a group assignment together with four fellow students. What is the probability that at least one of your group mates will underestimate his studies?-
print("sol")
print(1-binom.cdf(0,4,underestimate/total))

# Q.3.A Calculate from the data how many % of the students are at most one and a half standard deviations larger or smaller than the mean.
mean_length = studenq.Length.mean()
std_length = studenq.Length.std()
studentsinrange = len(studenq.loc[(studenq.Length >= mean_length-(1.5 * std_length)) & (studenq.Length<= mean_length+ (1.5 * std_length))])
print("sol")
print(studentsinrange/ len(studenq))

# Q.3.B What % do you expect for a normal distribution?
print("sol")
print(norm.cdf(1.5) -norm.cdf(-1.5))

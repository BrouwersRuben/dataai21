# P2
import pandas as pd
import math
from scipy.stats import t
from scipy.stats import binom
from scipy.stats import norm
from scipy.stats import ttest_1samp
from scipy.stats import chisquare
studenq= pd.read_csv('../Questionnaire 20-21.csv', delimiter=';',decimal=',')

# Q.1. Continue with last time's exercise Q.1: 
# Q.1.A Add a column to the data frame and place in it the length of a person expressed in his shoe size (i.e. length divided by shoe size)
numberofshoes= studenq.Length/ studenq["Shoe Size"]
# print("number of shoes: ")
# print(numberofshoes)
studenq["Length in shoe size"] = pd.Series(numberofshoes)
# Q.1.B Someone claims that the ratio of length to shoe size of a person is 4.2 with a standard deviation of 0.05. Based on the data from the questionnaire, can you agree with this? And this from which level of significance()?
mu=4.2
ttest_1samp(studenq["Length in shoe size"].dropna(axis=0), mu)

# Q.2.A Determine the 99% confidence interval for the distance to KdG (first remove the outliers!). 
def no_outliers(data):
    print(type(data))
    Q1 = data.quantile(0.25)
    Q3 = data.quantile(0.75)
    I = Q3 -Q1
    low = Q1 -1.5 * I
    high = Q3 + 1.5 * I
    print(low)
    print(high)
    return[data[data.between(low, high)]] 

distance= no_outliers(studenq["Travel Distance"])[0]
x_bar= distance.mean()
s = distance.std()
n = len(distance)
print(t.interval(alpha=0.99, df=n-1, loc=x_bar,scale=s/math.sqrt(n)))

# Q.2.B Someone claims that the distance to KdG is 7.5 km on average. From what level of significance () can you not refute the claim?
mu=7.5
print(ttest_1samp(distance,mu))

# Q.3 The table below shows the distribution of the world population according to the different blood groups
# Can you refute this on the basis of the sample?take = 0.05.
pd.crosstab(index=studenq['Blood Type'],columns=studenq['Resus Factor'])
observed_values = [6, 0, 3, 2, 1, 0, 0, 0]
expected_values= [0.3737*12, 0.2724*12, 0.2283*12, 0.0622*12, 0.0269*12, 0.0209*12, 0.0116*12, 0.004*12]
print(chisquare(observed_values, expected_values))
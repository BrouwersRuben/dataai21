# P1
import pandas as pd
import math
from scipy.stats import t
from scipy.stats import binom
from scipy.stats import norm
studenq= pd.read_csv('../Questionnaire 20-21.csv', delimiter=';',decimal=',')

# Q.1.A Add a column to the data frame and place in it the length of a person expressed in his shoe size (i.e. length divided by shoe size)
numberofshoes = studenq.Length/ studenq["Shoe Size"]
studenq["Length in shoe size"] = pd.Series(numberofshoes)

# Q.1.B Determine the mean and standard deviation 
x_bar = studenq["Length in shoe size"].mean()
s = studenq["Length in shoe size"].std()
print(s)

# Q.2 Specify the confidence interval (=5%) for the average ratio of length to shoe size
n = len(studenq["Length in shoe size"])
factor = t.ppf((1+0.95)/2, df= n-1)
interval = (x_bar-factor*s/math.sqrt(n), x_bar+factor*s/math.sqrt(n))
# OR
print(t.interval(alpha=0.95,df=n-1,loc=x_bar,scale=s/math.sqrt(n)))

#Q.3 Someone claims that the ratio of length to shoe size of a person is equal to 4.2 
# with a standard deviation of 0.05. Based on the data from the questionnaire, can you 
# agree with this (= 5%)? And if = 2.5% ?

# = 5% 
mu0 = 4.2
s0 = 0.05
factor = norm.ppf((1+0.95)/2)
interval = (mu0-factor* s0/math.sqrt(n), mu0+factor*s0/math.sqrt(n))
# OR
norm.interval(alpha=0.95,loc=mu0,scale=s0/math.sqrt(n))
# = 2.5% 
factor = norm.ppf((1+0.975)/2)
interval = mu0-factor* s0/math.sqrt(n), mu0+factor*s0/math.sqrt(n)
# OR
print(norm.interval(alpha=0.975,loc=mu0,scale=s0/math.sqrt(n)))
from random import Random
from inspyred import ec
import pandas as pd
import numpy as np
import math
import statistics

# Apply simulated annealing to the following problem:
from simanneal import Annealer


class ExerciseProblem(Annealer):
    def move(self):
        rand_fact = np.random.uniform(-1, 1)
        i = np.random.randint(0, 2)
        self.state[i] = np.random.uniform(-20, 20)
        if self.state[i] < -20:
            self.state[i] = -20
        if self.state[i] > 20:
            self.state[i] = 20

    def energy(self):
        fact1 = self.state[0]
        fact2 = self.state[1] + 9
        return -(fact1 * math.sin(math.sqrt(abs(fact1-fact2)))-fact2 * math.sin(math.sqrt(abs(fact2+fact1/2))))


init_sol = np.random.uniform(-20, 20, size=2)  # initial solution
exercise = ExerciseProblem(init_sol)
exercise.anneal()


studenq = pd.read_csv('../Questionnaire 20-21.csv', delimiter=';', decimal=',')


# Q1.A Create a function to calculate Kendall's median rank
# correlation coefficient with the following parameters:
#     rv –a vector (one dimensional array) consisting of ranking numbers
#     Rm –a matrix with ranking number

fruit = pd.read_csv('fruit_table.csv', delimiter=';')

rv = fruit.loc[:, 'Student00'].copy()
Rm = fruit.copy()

def medianRankCor(rank_vect, Rank_mat):  # rank_vect is of type pd.Series
    result = []
    for column in Rank_mat:
        corr_coef = Rank_mat[column].corr(rank_vect, method='kendall')
        result.append(corr_coef)
    return statistics.median(result)


# Q1.B Using a genetic algorithm, find the best ranking –of the
# fruits –that yields kendall's highest median ranking correlation
# coefficient with the students' specified rankings


#Problem specific components

def generate(random=None, args=None) -> []:
    pref_mat = args.get('pref_matrix', [])
    size = pref_mat.shape[0]
    sol = np.random.permutation(np.arange(1, size+1))
    solution = pd.Series(data=sol, name='a solution', index=pref_mat.index)
    return solution.tolist()


def evaluate(candidates, args={}):
    pref_mat = args.get('pref_matrix', [])
    fitness = []
    for candidate in candidates:
        cand = pd.Series(data=candidate, index=pref_mat.index)
        result = []
        for column in pref_mat:
            corr_coef = pref_mat[column].corr(cand, method='kendall')
            result.append(corr_coef)
        fitness.append(statistics.median(result))
    return fitness


#Meta-heuristic specific components
rand = Random()
ga = ec.GA(rand)
population: [ec.Individual] = ga.evolve(
    generator=generate,
    evaluator=evaluate,
    selector=ec.selectors.tournament_selection,  # optional
    tournament_size=10,  # optional
    pop_size=100,
    maximize=True,
    bounder=ec.Bounder(1, Rm.shape[0]),
    max_evaluations=20000,
    mutation_rate=0.01,
    pref_matrix=Rm)
population.sort(reverse=True)
print("Population: ")
print(population[0])

# Compare the solution you found with the optimal
# solution (obtained with the AURORA software) being:

opt_sol = [5, 1, 4, 9, 7, 2, 8, 6, 10, 3]
opt_sol = pd.Series(data=opt_sol, name = 'Optimal solution', index=Rm.index)
print("MedianRankCor: ")
print(medianRankCor(opt_sol, Rm))  # 0.4667


# Q2.B Apply your solutions for Q1 on the data concerning the
# class mascot (see data set ‘mascot_table.csv’ on Canvas)

# Compare the solution you found with the optimal solution
# (obtained with the AURORA software) being (1 out of 39
# optimal solutions with the same -0,333-median rank
# correlation coefficient):

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from tensorflow.keras.layers import Input, Dense, BatchNormalization
from tensorflow.keras.utils import to_categorical
from tensorflow import keras
from tensorflow.keras import Model
from tensorflow. keras.optimizers import Adam
from sklearn.model_selection import train_test_split
from tensorflow.python.keras.engine.base_layer_utils import collect_previous_mask

# 1.A Build a neural network –with one hidden layer containing one neuron less that the number
# of input neurons-to determine a student's opinion about the number of years Biden will stay
# in the white house based on the number of hours of math in the final year of high school, the
# number of mobile devices he/she uses, and the number of siblings. Split the data is a training
# dataset and a test dataset (90-10 ratio). Also normalize the data using the decimal scaling.

#Step 1: Upload the dataset and inspect the data
studenq = pd.read_csv('../Questionnaire 20-21.csv', delimiter=';', decimal=',')
studenq.info()
studenq.isna().sum().sum()
studenq.describe()  # note: visualisation of values only for quantitative variables

# Step 2: Perform the needed data management manipulations
subset = studenq[['Hours Math', 'Mobile Devices', 'Siblings', 'Biden']].copy()
subset['Hours Math'] = pd.to_numeric(studenq['Hours Math'], errors='coerce')
subset.loc[subset['Hours Math'] > 15,
           'Hours Math'] = np.nan   # remove unreal values
subset.dropna(inplace=True)
subset = subset.reset_index()  # if not impact on evaluation metrics
x_biden = subset[['Hours Math', 'Mobile Devices', 'Siblings']].copy()
y_biden = subset[['Biden']].copy()

# Step 3: Normalise the data


def min_max_norm(col):
    minimum = col.min()
    range = col.max() - minimum
    return (col-minimum)/range


def decimal_scaling_norm(col):
    maximum = col.max()
    tenfold = 1
    while (maximum > tenfold):
        tenfold = tenfold * 10
    return(col/tenfold)


def normalized_values(df, norm_funct):
    df_norm = pd.DataFrame()
    for column in df:
        df_norm[column] = norm_funct(df[column])
    return df_norm


x_biden_norm = normalized_values(
    x_biden, decimal_scaling_norm)  # home made function

# Step 4: Split the dataset into a training dataset and a test dataset
x_train_biden, x_test_biden, y_train_biden, y_test_biden = train_test_split(
    x_biden_norm, y_biden, test_size=0.1)
max_cat = y_biden['Biden'].max()+1
y_train_biden = to_categorical(
    y_train_biden, num_classes=max_cat, dtype="int64")
y_test_biden = to_categorical(y_test_biden, num_classes=max_cat, dtype="int64")

# Step 5: Build the ANN-model
### Preparing the layers of the neural network
inputs_b = Input(shape=(3,))
x_b = Dense(6, activation='sigmoid')(inputs_b)
x_b = Dense(12, activation='sigmoid')(x_b)
x_b = Dense(24, activation='sigmoid')(x_b)
outputs_b = Dense(max_cat, activation='softmax')(x_b)
### Build the neural network model
model_biden = Model(inputs_b, outputs_b, name='Biden')
model_biden.summary()
model_biden.compile(
    optimizer=Adam(learning_rate=0.01),
    loss=keras.losses.categorical_crossentropy,
    metrics=['accuracy'])

# Step 6: Train the ANN-model
history_biden = model_biden.fit(
    x_train_biden,  # training data
    y_train_biden,  # training targets
    epochs=200)

# 1.B Retake the neural network that determines a student's opinion of the number of years Biden
# will stay in the white house (see 1.A). Draw the ROC curve, create the confusion matrix and
# calculate the most commonly used evaluation metrics.

# Step 7: Evaluate the quality of the ANN-model
predicted = pd.Series(np.argmax(model_biden.predict(x_train_biden), axis=1), name='predicted')
actual = pd.Series(np.argmax(y_train_biden, axis=1), name='actual')
conf_mat = pd.crosstab(index=actual, columns=predicted)

from sklearn.metrics import classification_report

# accuracy(conf_mat)
# precision(conf_mat)
# recall(conf_mat)
# overviewmetrics(conf_mat, 1.0)

target_names = ['Col_1', 'Col_2', 'Col_3']
print(classification_report(actual, predicted,
      target_names=target_names, digits=4))

#ROC curve can not be drawn, no binary but multi-class classifier!



import pandas as pd
import numpy as np
from sklearn.metrics import confusion_matrix, accuracy_score, precision_recall_fscore_support, roc_curve, roc_auc_score
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
import matplotlib.pyplot as plt


def plot_roc(y_true, y_score, title='ROC Curve', **kwargs):
    from sklearn.metrics import roc_curve, roc_auc_score
    if'pos_label' in kwargs:
        fpr, tpr, thresholds = roc_curve(
            y_true=y_true, y_score=y_score, pos_label=kwargs.get('pos_label'))
        auc = roc_auc_score(y_true, y_score)
    else:
        fpr, tpr, thresholds = roc_curve(y_true=y_true, y_score=y_score)
        auc = roc_auc_score(y_true, y_score)

    # calculate optimal cut-off with Youden index method
    optimal_idx = np.argmax(tpr-fpr)
    optimal_threshold = thresholds[optimal_idx]

    figsize = kwargs.get('figsize', (7, 7))
    fig, ax = plt.subplots(1, 1, figsize=figsize)
    ax.grid(linestyle='--')

    # plot ROC curve
    ax.plot(fpr, tpr, color='darkorange', label='AUC: {}'.format(auc))
    ax.set_title(title)
    ax.set_xlabel('FalsePositiveRate(FPR)')
    ax.set_ylabel('True PositiveRate(TPR)')
    ax.fill_between(fpr, tpr, alpha=0.3, color='darkorange', edgecolor='black')

    # plot classifier
    ax.plot([0, 1], [0, 1], color='navy', lw=2, linestyle='--')

    # plot optimal cut-off
    ax.scatter(fpr[optimal_idx], tpr[optimal_idx], label='optimalcutoff {:.2f} op ({:.2f},{:.2f})'.format(
        optimal_threshold, fpr[optimal_idx], tpr[optimal_idx]), color='red')
    ax.plot([fpr[optimal_idx], fpr[optimal_idx]], [
            0, tpr[optimal_idx]], linestyle='--', color='red')
    ax.plot([0, fpr[optimal_idx]], [tpr[optimal_idx],
            tpr[optimal_idx]], linestyle='--', color='red')
    ax.legend(loc='lower right')

    plt.show()

def show_confusion_table(confusion_matrix, labels):
    fig, ax = plt.subplots(figsize=(5, 5))
    ax.matshow(confusion_matrix, cmap=plt.cm.Oranges, alpha=0.3)
    for i in range(confusion_matrix.shape[0]):
        for j in range(confusion_matrix.shape[1]):
            ax.text(
                x=j, y=i, s=confusion_matrix[i, j], va='center', ha='center', size='xx-large')
    plt.xlabel('Predictions', fontsize=18)
    plt.ylabel('Actuals', fontsize=18)
    ax.set_xticks(range(0, len(labels)))
    ax.set_xticklabels(labels)
    ax.set_yticks(range(0, len(labels)))
    ax.set_yticklabels(labels)
    plt.title('Confusion Matrix', fontsize=18)
    plt.show()

studenq = pd.read_csv('../Questionnaire 20-21.csv',
                      delimiter=';', decimal=',')
studenq.describe()
df = pd.DataFrame()

df = studenq.loc[:, ['Mobile Devices',
                     'Hours Math', 'Internet Purchase']].copy()

df['Hours Math'] = pd.to_numeric(studenq['Hours Math'], errors='coerce')
df.loc[df['Hours Math'] > 15, 'Hours Math'] = np.nan

df.loc[df['Internet Purchase'] == 'No', 'Internet Purchase'] = 0
df.loc[df['Internet Purchase'] == 'Yes', 'Internet Purchase'] = 1
df['Internet Purchase'] = pd.to_numeric(df['Internet Purchase'])
df.info()
df.describe()

# Q.1.B In the questionnaire you want to predict the "importance of AI
# study". This is an ordinal fact that you will have to reduce to two
# possible values: anything equal or more than "Great importance" is
# "large", everything smaller is "moderate". Put this as a column in
# the dataframe df
studenq['Importance AI Study'] = studenq['Importance AI Study'].astype(
    pd.CategoricalDtype(categories=['Not at all', 'Little importance', 'Moderate importance', 'Great importance',
                                    'Extreemly important'], ordered=True))
studenq.info()

importance = pd.Series(index=range(len(studenq)), dtype='category').astype(
    pd.CategoricalDtype(categories=['moderate', 'large']))

for i in range(0, len(studenq)):
    if studenq.loc[i, 'Importance AI Study'] < 'Great importance':
        importance[i] = 'moderate'
    else:
        importance[i] = 'large'

df['Importance AI Study'] = importance

model = LinearDiscriminantAnalysis()
df = df.dropna(axis='rows')
X = df.loc[:, ['Mobile Devices', 'Hours Math', 'Internet Purchase']].copy()
y = df['Importance AI Study'].copy()
model.fit(X, y)

predicted = model.predict(X)


conf_matrix = confusion_matrix(y_true=y, y_pred=predicted)
show_confusion_table(conf_matrix, y.unique().tolist())

accuracy_score(y_true=y, y_pred=predicted)
precision_recall_fscore_support(y_true=y, y_pred=predicted, beta=1.0)

y_score = model.predict_proba(X)[:, 0]
plot_roc(y, y_score, pos_label='large')
fpr, tpr, thresholds = roc_curve(y_true=y, y_score=y_score, pos_label='large')
optimal_idx = np.argmax(tpr - fpr)
optimal_threshold = thresholds[optimal_idx]
auc = roc_auc_score(y, y_score)

print("FPR: "  + str(np.argmax(fpr)))
print("tpr: " + str(np.argmax(tpr)))
print("optimal_idx: " + str(np.argmax(tpr-fpr)))
print("optimal_threshold: " + str(optimal_threshold))
print("auc: " + str(auc))

# Association Rules

import pandas as pd
import numpy as np
from mlxtend.frequent_patterns import association_rules, apriori

# Each student buys in the store the 3 fruits that he prefers to eat
# (see fruit purchases.csv). Which rules do you obtain –use the apriori 
# algorithm to determine the item sets, and use min. support = 0.2 and min. 
# threshold for confidence=0.5-? Which fruit should be close to one another in the shop?

purchases= pd.read_csv('Fruitpurchases.csv', delimiter=';', decimal=',')
purchases.pop('NameBuyer')
itemsets = apriori(purchases, min_support=0.2, use_colnames=True)
print(itemsets)
rules = association_rules(itemsets, metric='confidence', min_threshold=0.5)
print(rules)

def AssociationRules(data, minSupport, minThreshold, metric):
    # Todo, before you do this, pop the columns you do not want to use
    from mlxtend.frequent_patterns import association_rules, apriori

    itemsets = apriori(data, min_support=minSupport, use_colnames=True)
    print(itemsets)

    rules = association_rules(
        itemsets, metric=metric, min_threshold=minThreshold)
    print(rules)


AssociationRules(purchases, 0.2, 0.5, 'confidence')

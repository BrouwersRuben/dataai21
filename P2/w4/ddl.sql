-- Q1
-- no_persons may only have values between 4 and 24
ALTER TABLE cottagetypes
    ADD CONSTRAINT cc_cottype_nopers CHECK ((no_persons BETWEEN 4 AND 24));
-- DOES NOT WORK :(

-- A
ALTER TABLE cottagetypes
    ADD CONSTRAINT cc_cottype_nopers CHECK ((no_persons BETWEEN 4 AND 24)) NOT VALID;

--B
-- Change the cottage that does not meat the constraint
UPDATE cottagetypes
SET no_persons = 4
WHERE no_persons <4;

-- C
-- activate constraint
ALTER TABLE cottagetypes
    VALIDATE CONSTRAINT cc_cottype_nopers;

-- D
-- check constraint
SELECT con.*
FROM pg_catalog.pg_constraint con INNER JOIN pg_catalog.pg_class rel
    ON rel.oid = con.conrelid
    INNER JOIN pg_catalog.pg_namespace nsp
        ON nsp.oid = connamespace
WHERE rel.relname = 'cottagetypes';

-- Q2
-- constraint can also be temporarily placed out of action (NOT WITH CHECK)
ALTER TABLE cottagetypes ALTER CONSTRAINT
    fk_cottype_park DEFERRABLE;

BEGIN;
SET CONSTRAINTS fk_cottype_park DEFERRED;
UPDATE cottagetypes SET park_code='FS'
WHERE park_code = 'SF' AND typeNo = '14';
COMMIT;

-- fk is still referenced from other table, where the constraint is still on... so error there

-- Q3
/* Add attribute at the end of the table */
ALTER TABLE clients
    ADD email VARCHAR(30);

/* Create an auxilary table */
CREATE TABLE auxiliaryTable
AS (SELECT clientno, last_name, first_name, email, street, houseno, postcode, city, status FROM clients);

/* drop table clients */
DROP TABLE clients CASCADE;/* all FK’s in other tables were also deleted */

/* rename the auxilary tables */
ALTER TABLE auxiliaryTable RENAME TO clients;

/* rebuild all the constraints in the clients table */
ALTER TABLE clients
    ADD CONSTRAINT pk_clients primary key(clientno);
ALTER TABLE clients
    ADD CONSTRAINT city_caps CHECK(city=UPPER(city));
ALTER TABLE clients
    ADD CONSTRAINT clientstreet_caps CHECK(street=UPPER(street));
ALTER TABLE clients
    ADD CONSTRAINT firstnm_caps CHECK(first_name=UPPER(first_name));
ALTER TABLE clients
    ADD CONSTRAINT lastnm_caps CHECK(last_name=UPPER(last_name));

/* rebuild foreign keys in the other tables */
 ALTER TABLE reservations
     ADD CONSTRAINT fk_res_clients FOREIGN KEY(clientno) REFERENCES clients (clientno);

-- Q4
-- reservation table == composite primary key
-- change table structure
-- reservation = resID

/* create an empty auxilary table */
CREATE TABLE reservations2 AS
    SELECT * FROM reservations;

TRUNCATE reservations2;
/* add identity column */
ALTER TABLE reservations2
    ADD resID INTEGER GENERATED ALWAYS AS IDENTITY;

/* copy the data from reservations into reservations2 */
INSERT INTO reservations2 (resNo, taNo, clientNo, park_code, houseNo, booking_date, start_date, end_date, reser_code, status, promo_code)
    SELECT resNo, taNo, clientNo, park_code, houseNo, booking_date, start_date, end_date, reser_code, status, promo_code
    FROM reservations;

/* Add primary key to the reservations2 table */
ALTER TABLE reservations2
    ADD CONSTRAINT pk_reservations_idcol PRIMARY KEY (resID);

/* find out these foreing keys: */
ALTER TABLE payments
    DROP CONSTRAINT fk_payment_reserv;

/* drop foreign keys refering to the primary key of the reservation table */
ALTER TABLE reservations DROP CONSTRAINT pk_reservations;

/* replace reservations table by reservations2 table */
DROP TABLE reservations;

ALTER TABLE reservations2
    RENAME TO reservations;

/* Add the foreign keys to the other tables -see drop above- */
ALTER TABLE payments ADD
    resID INTEGER;

UPDATE payments p
    SET resID = (SELECT resID
                FROM reservations r
                WHERE p.resno = r.resNo AND p.taNo=r.taNo);

ALTER TABLE payments
    ADD CONSTRAINT fk_payment_reserv FOREIGN KEY (resID) REFERENCES reservations(resID);

/* drop the columns in payments table that were used by the former foreign key and that have no use anymore */
ALTER TABLE payments
    DROP COLUMN resNo;

ALTER TABLE payments
    DROP COLUMN taNo;

-- Q5
-- Each reservation is given a unique number RES_ID via a sequence SEQ_RESERVATIES
/* step 1: make a sequence */
CREATE SEQUENCE seq_reservations
    START WITH 1 INCREMENT BY 1;

/* step 2: add column seq_resId and populate it*/
ALTER TABLE reservations
    ADD seq_resID INTEGER DEFAULT nextval('seq_reservations');

/* step 3: drop old primary key */
ALTER TABLE reservations
    DROP CONSTRAINT pk_reservations CASCADE;

/* step 4: create new primary key */
ALTER TABLE reservations
    ADD CONSTRAINT pk_reservations PRIMARY KEY (seq_resID);

/* step 5: create new foreign key in table payments -was drop in step 3 because of the CASCADE option */
ALTER TABLE payments
    ADD seq_res_id INTEGER CONSTRAINT fk_payments_reserv REFERENCES reservations;

/* step 6: populate new foreign key attribute in table payments*/
UPDATE payments p
    SET seq_res_id=(SELECT seq_resid
                    FROM reservations r
                    WHERE p.resNo=r.resNo AND p.taNo=r.taNo);

/* step 7: drop the column in payments table that were used by the former foreign key and that has no use anymore */
ALTER TABLE payments
    DROP COLUMN resNo;

ALTER TABLE payments
    DROP COLUMN taNo;

-- Q6
-- A
-- Make update
UPDATE cottages
SET houseno = 52
WHERE park_code = 'SF'
  AND typeNo = '12'
  AND houseNo = 50;
/* duplicate key (already exists) */
SELECT *
FROM cottages
WHERE park_code = 'SF'
  AND typeNo = '12';

-- B
BEGIN;

/* drop the primary key */
ALTER TABLE cottages
    DROP CONSTRAINT pk_cottage CASCADE;
/* note: dependency fk_res_cottage */

/* add the primary key but with the parameter DEFERRABLE*/
ALTER TABLE cottages
    ADD CONSTRAINT pk_cottage PRIMARY KEY (park_code, typeNo,houseNo) DEFERRABLE;

/* Put the primary key on the cottages table temporarily out of operation */
ALTER TABLE cottages DISABLE TRIGGER ALL;


-- C
SELECT *
FROM cottages
WHERE park_code = 'SF'
  AND typeNo = '12';

-- D
ALTER TABLE cottages ENABLE PRIMARY KEY; /* <-- Why doesn't this work*/
ALTER TABLE cottages ENABLE TRIGGER ALL;
ROLLBACK;
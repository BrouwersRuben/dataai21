-- Q1
-- indexes on the tables
SELECT tablename, indexname, indexdef
FROM pg_indexes;

-- All of the indexes are automatically created by postgres

-- the unique indexes on the PK, have the same name as the pk constraint

-- Q2
-- Make PK on cottagetypes on the park_code and typeNo (unique index on combination of
-- attributes)

-- Check the order
SELECT tablename, indexname, indexdef
FROM pg_indexes
WHERE indexname = 'pk_cottype';

-- Order ok?
-- For the best performance, place the attribute with the greatest variety of values as first.
-- In this case, that is typeNo => so better to change the order.

-- Q3
-- index on table cottage_prices
SELECT tablename, indexname, indexdef
FROM pg_indexes
WHERE tablename = 'cottype_prices';

-- deactivate pk constraint
BEGIN;

ALTER TABLE cottype_prices
    DISABLE TRIGGER ALL;

-- look at indexes again
SELECT tablename, indexname, indexdef
FROM pg_indexes
WHERE tablename = 'cottype_prices';
/* The index is still there */

-- reactivate
ALTER TABLE cottype_prices
    ENABLE TRIGGER ALL;

COMMIT;

-- Delete the index
DROP INDEX pk_cottype_prices;
/* You cannot delete an index that is automatically created
   by a PK (or UNIQUE) constraint with DROP INDEX.
   You can only delete the index by removing the constraint
*/

-- Q4
-- Create tablespace on other disk/partition/folder
CREATE TABLESPACE holidaypark_ts
    LOCATION 'C:\temp\Postgress\HolidayPark';

-- clients_copy as a copy of the table client. Check the indexes
CREATE TABLE clients_copy AS
    SELECT *
    FROM clients;

SELECT *
FROM pg_indexes
WHERE tablename = 'clients_copy';

ALTER TABLE clients_copy
    ADD CONSTRAINT pk_clients_copy PRIMARY KEY (clientNo);

-- which tablespace was the newly created index
SELECT *
FROM pg_indexes
WHERE tablename = 'clients_copy';

-- move clients_copy to recently created tablespace
ALTER TABLE clients_copy
    SET TABLESPACE holidaypark_ts;

-- check if moved successfully
SELECT *
FROM pg_tables
WHERE tablename = 'clients_copy';

--> index still in <<null>>

-- Q5
EXPLAIN SELECT *
FROM clients
WHERE clientNo = '77777';

--> Index is used

-- Q6
-- Create an index ind_fname_client on the first_name attribute from the clients table.
CREATE INDEX ind_fname_client ON clients (first_name);
-- A
EXPLAIN SELECT clientNo,first_name,last_name
FROM clients
WHERE first_name IS NULL;
-- Not used (sequential scan)

-- B
EXPLAIN SELECT * FROM clients WHERE first_name='WILLY';
-- index is NOT used. Reason: PostgreSQL could use the index but estimates that using the index will not result in a
-- faster executing (occurs regularly when small tables are involved).

-- C
EXPLAIN SELECT clientNo, first_name, last_name
FROM clients
WHERE UPPER(first_name) = 'WILLY';
-- Not index = function is applied to the indexed attribute

-- D
EXPLAIN SELECT *
FROM clients
WHERE first_name LIKE '%TH%';
-- Index not used

-- E
-- only in c. and d. will the index NOT be used

-- Q7
-- Do above queries again with seq off
SET enable_seqscan = off;
-- Redo Q6 queries, what happens?
SET enable_seqscan = on;

-- Q8
-- ind_lname_client on the last_name attribute from the clients table
CREATE INDEX ind_lname_client ON clients (last_name);
SET enable_seqscan = off;

--A
EXPLAIN SELECT *
FROM clients
WHERE last_name ='STOOT';
-- index scan used, with on --> seq (because cheaper)

--B
EXPLAIN SELECT * FROM clients
WHERE UPPER(last_name)='STOOT';
-- seq scan used, because attribute in function

-- C
-- Ensure index is used on above stuff
/* create a function based index: */
CREATE INDEX ind_lname_client_upper ON clients(UPPER(last_name));

EXPLAIN SELECT *
FROM clients
WHERE UPPER(last_name)='STOOT';

SET enable_seqscan = on;

-- Q9
-- A
-- check if there is index on reservations(clientNo)
CREATE INDEX ind_client_res ON reservations(clientNo);

-- B
-- the number of reservations per customer, sorted by customer number
EXPLAIN SELECT clientNo,COUNT(*) FROM reservations GROUP BY clientNo ORDER BY clientNo;
-- index not used

DROP INDEX ind_fname_client;
DROP INDEX ind_lname_client;
DROP INDEX ind_client_res;

-- Q10 (see these in dalibo)
-- A
EXPLAIN SELECT c.clientNo, c.first_name,c.last_name
FROM clients c JOIN reservations r
    ON r.clientNo = c.clientNo;
-- Clients is outer table (scanned first)
-- hash join is used

-- B
EXPLAIN SELECT c.clientNo, c.first_name,c.last_name
FROM reservations r JOIN clients c
    ON c.clientNo = r.clientNo;
-- same as A

-- C
EXPLAIN SELECT c.last_name, c.first_name,p.sport, p.park_name, r.typeNo, r.houseNo
FROM reservations r JOIN clients c
    ON c.clientNo = r.clientNo
    JOIN parks p
        ON p.code = r.park_code
WHERE p.country_code = '1';
-- parks is outer table

-- D
EXPLAIN SELECT pa.park_code,pa.attraction_code,pat.description
FROM parkattractions pa JOIN parkattractiontypes pat
    ON pa.attraction_code = pat.attraction_code;

-- C
-- cottagetypes is outer table
-- hash join is used

-- Q11
-- 1
EXPLAIN SELECT *
FROM travelagencies
WHERE taNo NOT IN
      (SELECT taNo FROM reservations);
-- sequential scans on both tables

-- 2
EXPLAIN SELECT *
FROM travelagencies t
WHERE NOT EXISTS (SELECT 'x' FROM reservations WHERE t.taNo = taNo);
-- hash join with reservations as outer table

--3
EXPLAIN SELECT t.taNo
FROM travelagencies t
EXCEPT (SELECT r.taNo FROM reservations r);
-- Hash set operation with sequal scan for each subquery

-- Q12
SET enable_nestloop = off;
SET enable_hashjoin = off;
SET enable_mergejoin = on;

EXPLAIN SELECT *
FROM reservations r JOIN clients c
    ON r.clientNo = c.clientNo;

SET enable_nestloop = on;
SET enable_hashjoin = off;
SET enable_mergejoin = off;

EXPLAIN SELECT *
FROM reservations r JOIN clients c
    ON r.clientNo = c.clientNo;

SET enable_nestloop = on;
SET enable_hashjoin = on;
SET enable_mergejoin = on;
-- Q1
-- Find everything in pg_tables
SELECT *
FROM pg_tables;

-- Find everything in information schema view tables
SELECT *
FROM information_schema.tables;

-- Find the requested information of a. by querying one of the information schema tables.
SELECT *
FROM information_schema.columns
WHERE table_name = 'pg_tables';

SELECT *
FROM information_schema.columns
WHERE table_name = 'tables';

-- What are the columns that both views have in common?
-- pg_tables   information_schema.tables
-- schemaname  table_schema
-- tablename   table_name

-- Q2
-- Look at the system catalog pg_sequence
SELECT *
FROM pg_sequence;

-- Create a new sequence test_sequence
CREATE SEQUENCE test_sequence
    INCREMENT BY 1
    START WITH 1;

SELECT *
FROM pg_sequence;

-- See what the current value of the sequence test_sequence is.
SELECT currval('test_sequence');
SELECT last_value FROM test_sequence;
SELECT last_value FROM pg_sequences;

-- Make increment by 5 instead of 1
UPDATE pg_sequence
SET seqincrement = 5
WHERE seqrelid = '67087';
-- need to know this seqrelid

-- Can you change the increment in this table
SELECT *
FROM pg_sequences;

-- No, this is view, not table
UPDATE pg_sequences SET increment_by = 10 WHERE sequencename = 'test_sequence';

-- Other way that can give us info about a sequence
-- YES ==> Info Schema view sequences:
SELECT *
FROM information_schema.sequences;

-- Q3
-- Where can you find the info about which views were created on this db
-- pg_views & inforamtion_schema.views:
SELECT *
FROM pg_views;

SELECT *
FROM information_schema.views
WHERE table_catalog = 'HolidayPark';

-- Q4
-- What info in pg_locks
-- Locks currently held or awaited

-- Where (System view) can you find the name of the relation
-- pg_class: OID attribute hold the relation id number while relname attribute holds the name of the relation / table.

-- Look at available locks
SELECT *
FROM pg_locks;

SELECT *
FROM pg_class
WHERE OID = '12290'; /* values can be different in your DB */

-- Start a transaction & change name of country with code 2 from "Netherlands" to "The Netherlands", check locks, Rollback transaction
BEGIN;
UPDATE countries
SET country_name = 'The Netherlands'
WHERE country_code = '2';

SELECT *
FROM pg_locks;

SELECT *
FROM pg_class
WHERE OID IN ('12290', '19971', '19968'); /* values can be different in your DB */

ROLLBACK;

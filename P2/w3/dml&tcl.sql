-- Q1
-- A
-- Add yourself to clients
BEGIN;

SELECT *
FROM clients;

INSERT INTO clients (clientno, last_name, first_name, street, houseno, postcode, city)
    VALUES ('99999', 'BROUWERS','RUBEN', 'SERINGENLAAN', 35, '2940', 'HOEVENEN');

SAVEPOINT new_client;

EXPLAIN SELECT *
FROM clients
WHERE first_name = 'RUBEN';

-- B
-- book 2 reservations
SELECT *
FROM reservations
ORDER BY resno;

INSERT INTO reservations (resno, tano, clientno, park_code, typeno, houseno, start_date, end_date)
    VALUES ( '99', '1', '99999',(SELECT code FROM parks WHERE upper(park_name) = 'WEERTERBERGEN'),'FK', '225', to_date('1/11/2021', 'DD/MM/YYYY'), to_date('05/11/2021', 'DD/MM/YYYY'));

INSERT INTO reservations (resno, tano, clientno, park_code, typeno, houseno, start_date, end_date)
    VALUES ( '100', '1', '99999',(SELECT code FROM parks WHERE upper(park_name) = 'WEERTERBERGEN'),'FV10', '515', to_date('27/12/2019', 'DD/MM/YYYY'), to_date('31/12/2019', 'DD/MM/YYYY'));

SAVEPOINT new_reservations;

SELECT *
FROM reservations
WHERE clientno = '99999';

-- C
-- Making a payment
INSERT INTO payments (paymentno, resno, tano, payment_date, amount, payment_method)
    VALUES ( (SELECT MAX(paymentno)+1 FROM payments), '99', '1', current_date, 200, 'B');

-- D
-- Rollbacking
ROLLBACK TO SAVEPOINT new_reservations;

SELECT *
FROM payments;

SELECT *
FROM reservations
WHERE clientno = '99999';

ROLLBACK TO SAVEPOINT new_client;

SELECT *
FROM reservations
WHERE clientno = '99999';

SELECT *
FROM clients
WHERE clientno = '99999';

ROLLBACK;

SELECT *
FROM clients
WHERE clientno = '99999';

END;

-- Q2
-- A
-- view 'pieter stoot' reservations
SELECT *
FROM reservations
WHERE clientno = (SELECT clientno
                FROM clients
                WHERE last_name = UPPER('stoot')
                  AND first_name= UPPER('pieter'));

-- B
-- give type of cottages with the most bathrooms
EXPLAIN SELECT park_code, typeno, no_bathrooms
FROM cottagetypes
WHERE no_bathrooms = (SELECT MAX(no_bathrooms)
                      FROM cottagetypes);

-- C
DELETE FROM clients
WHERE last_name = UPPER('stoot')
  AND first_name = UPPER('pieter') ;
/* Does not work, FK */

BEGIN;

ALTER TABLE reservations
    DROP CONSTRAINT fk_res_client;

DELETE FROM clients
WHERE last_name = UPPER('stoot')
  AND first_name = UPPER('pieter');

SELECT *
FROM clients
WHERE last_name = UPPER('stoot')
  AND first_name= UPPER('pieter');

ROLLBACK;

-- D
BEGIN;

-- Look at reservations for FK 255 WB
SELECT *
FROM reservations
WHERE park_code = 'WB'
  AND houseno = '225';

-- Remove this cottage
SELECT park_code, typeno, houseno
FROM cottages
WHERE typeno like 'FK%';

DELETE FROM cottages
WHERE park_code = 'WB'
  AND typeno= 'FK'
  AND houseno = '225';
/* Will work, there are no FK on the reservations table*/

SELECT park_code, typeno, houseno
FROM cottages
WHERE typeno like 'FK%';

ROLLBACK;

-- E
-- Do everything again, in 1 transaction
BEGIN;

INSERT INTO clients (clientno, last_name, first_name, street, houseno, postcode, city)
       VALUES ('99999', 'DE KEYSER','WIM', 'RED NOSES STREET', 11, '2021', 'ANTWERPEN');

INSERT INTO reservations (resno, tano, clientno, park_code, typeno, houseno, start_date, end_date)
    VALUES ( '99', '1', '99999',(SELECT code FROM parks WHERE upper(park_name) = 'WEERTERBERGEN'),'FK', '225', to_date('1/11/2021', 'DD/MM/YYYY'), to_date('05/11/2021', 'DD/MM/YYYY'));

INSERT INTO reservations (resno, tano, clientno, park_code, typeno, houseno, start_date, end_date)
    VALUES ( '100', '1', '99999',(SELECT code FROM parks WHERE upper(park_name) = 'WEERTERBERGEN'),'FV10', '515', to_date('27/12/2019', 'DD/MM/YYYY'), to_date('31/12/2019', 'DD/MM/YYYY'));

DELETE FROM cottages
WHERE park_code = 'WB'
  AND typeno= 'FK'
  AND houseno = '225';

ROLLBACK;

-- Q3
-- change the price weekend and midweek for all types of cottages in the Netherlands for summer holidays in 2021 with 10%
BEGIN;

SELECT price_midweek, price_weekend, park_code, season_code
FROM cottype_prices
WHERE season_code = (SELECT code
                    FROM seasons
                    WHERE LOWER(description) = 'summer holidays 2021')
  AND park_code IN (SELECT code
                    FROM parks p JOIN countries c
                        ON p.country_code = c.country_code
                    WHERE LOWER(c.country_name)='netherlands');

UPDATE cottype_prices
SET price_weekend=price_weekend*1.1, price_midweek=price_midweek*1.1
WHERE season_code = (SELECT code
                    FROM seasons
                    WHERE LOWER(description) = 'summer holidays 2021')
  AND park_code IN (SELECT code
                    FROM parks p JOIN countries c
                        ON p.country_code = c.country_code
                    WHERE LOWER(c.country_name)='netherlands');

ROLLBACK;

-- Q4

# Discriminant analysis
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis

biopsy = pd.read_csv('biopsy.csv')
X = biopsy[['V1','V2', 'V3']]
y = biopsy['class']

# This is how you make an object in py
lda = LinearDiscriminantAnalysis()
lda.fit(X,y)

lda.classes_  # values of the dependent variable
lda.coef_  # coefficients of the discriminant function
lda.priors_  # probability of an observation coming from a particular group
lda. explained_variance_ratio_  # how much of the variance is explained
# by each of the discriminant functions (only useful when there are several 
# # discriminant functions

# map the independent variables based on the discriminant functions of the model to their N 
# # discriminant values
LD = lda.transform(X)

# combine with the original dependent variable
LD_df= pd.DataFrame(zip(LD[:,0], biopsy['class']), columns=['LD1','Target'])

LD_df.hist(column=['LD1'], by='Target', bins=25, density=True, edgecolor='black' ,color='cyan', sharex=True, sharey=True, figsize=(10,10), layout=(2,1))
fig, ax = plt.subplots(figsize=(10,5))
LD_df['LD1'][LD_df['Target'] == 'benign'].hist(ax=ax, bins=25, density = True,edgecolor='black', color='green', label='benign')
LD_df['LD1'][LD_df['Target'] == 'malignant'].hist(ax=ax, bins=25, density = True,edgecolor='black', color='red', alpha=0.7, label='malignant')
ax.legend()
ax.grid(False)
plt.show()
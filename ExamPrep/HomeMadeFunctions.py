from typing import MappingView


def plotbinom(n, p):
    x = range(0, n+1)
    plt.figure()
    fig, ax = plt.subplots(1, 1)
    ax.plot(x, binom.pmf(x, n, p), 'bo', ms=0)
    ax.vlines(x, 0, binom.pmf(x, n, p), colors='b', lw=15)
    plt.show()
    return

def no_outliers(data):
    print(type(data))
    Q1 = data.quantile(0.25)
    Q3 = data.quantile(0.75)
    I = Q3 - Q1
    low = Q1 - 1.5 * I
    high = Q3 + 1.5 * I
    print(low)
    print(high)
    return[data[data.between(low, high)]]


def createBarChart(data, x_values, y_values, graphName):
    fig = plt.figure()
    plt.title(graphName)
    ax = fig.add_axes([0, 0, 1, 1])
    ax.bar(data[x_values], data[y_values])
    plt.show()

def showBarChart():
    #Sorted by highest support first
    itemsets = apriori(adultUCItransactopnal, min_support=0.1,
                       use_colnames=True).sort_values(by=['support'], ascending=False)
    #print(itemsets)
    #Convert to string or else error
    itemsets['itemsets'] = itemsets['itemsets'].astype('|S')
    fig = plt.figure()
    ax = fig.add_axes([0, 0, 1, 1])
    ax.bar(itemsets['itemsets'], itemsets['support'])
    plt.show()


def filter_rules_by_antecedents(row, antecedents):
    return row['antecedents'].issuperset(antecedents)

def associationRulesApriori(data, min_support, min_threshold, minlen, maxlen):
    #e. Convert the dataframe to a transactional format with Pandas get_dummiesfunction. Also use the parameter prefix_sep='='. Take a look at the result.
    #adultUCItransactopnal = pd.get_dummies(adultUCIDroppedColumns, prefix_sep='=')
    def rule_filter(row, minlen, maxlen):
        length = len(row['antecedents']) + len(row['consequents'])
        return minlen <= length <= maxlen

    itemsets = apriori(data, min_support=min_support, use_colnames=True)

    rules = association_rules(
        itemsets, metric='confidence', min_threshold=min_threshold)

    return rules[rules.apply(lambda row: rule_filter(row, minlen, maxlen), axis=1)].sort_values(by='confidence', ascending=False)


def plot_roc(y_true, y_score, title='ROC Curve', **kwargs):
    from sklearn.metrics import roc_curve, roc_auc_score
    if'pos_label' in kwargs:
        fpr, tpr, thresholds = roc_curve(
            y_true=y_true, y_score=y_score, pos_label=kwargs.get('pos_label'))
        auc = roc_auc_score(y_true, y_score)
    else:
        fpr, tpr, thresholds = roc_curve(y_true=y_true, y_score=y_score)
        auc = roc_auc_score(y_true, y_score)

    # calculate optimal cut-off with Youden index method
    optimal_idx = np.argmax(tpr-fpr)
    optimal_threshold = thresholds[optimal_idx]

    figsize = kwargs.get('figsize', (7, 7))
    fig, ax = plt.subplots(1, 1, figsize=figsize)
    ax.grid(linestyle='--')

    # plot ROC curve
    ax.plot(fpr, tpr, color='darkorange', label='AUC: {}'.format(auc))
    ax.set_title(title)
    ax.set_xlabel('FalsePositiveRate(FPR)')
    ax.set_ylabel('True PositiveRate(TPR)')
    ax.fill_between(fpr, tpr, alpha=0.3, color='darkorange', edgecolor='black')

    # plot classifier
    ax.plot([0, 1], [0, 1], color='navy', lw=2, linestyle='--')

    # plot optimal cut-off
    ax.scatter(fpr[optimal_idx], tpr[optimal_idx], label='optimalcutoff {:.2f} op ({:.2f},{:.2f})'.format(
        optimal_threshold, fpr[optimal_idx], tpr[optimal_idx]), color='red')
    ax.plot([fpr[optimal_idx], fpr[optimal_idx]], [
            0, tpr[optimal_idx]], linestyle='--', color='red')
    ax.plot([0, fpr[optimal_idx]], [tpr[optimal_idx],
            tpr[optimal_idx]], linestyle='--', color='red')
    ax.legend(loc='lower right')

    plt.show()


def show_confusion_table(confusion_matrix, labels):
    fig, ax = plt.subplots(figsize=(5, 5))
    ax.matshow(confusion_matrix, cmap=plt.cm.Oranges, alpha=0.3)
    for i in range(confusion_matrix.shape[0]):
        for j in range(confusion_matrix.shape[1]):
            ax.text(
                x=j, y=i, s=confusion_matrix[i, j], va='center', ha='center', size='xx-large')
    plt.xlabel('Predictions', fontsize=18)
    plt.ylabel('Actuals', fontsize=18)
    ax.set_xticks(range(0, len(labels)))
    ax.set_xticklabels(labels)
    ax.set_yticks(range(0, len(labels)))
    ax.set_yticklabels(labels)
    plt.title('Confusion Matrix', fontsize=18)
    plt.show()


def showMetrics(TP, FN, FP, TN):

    def Accuracy(TP, TN, FP, FN):
        total = TP + TN + FP + FN
        accuracy = (TP + TN) / total
        return accuracy

    def PrecisionPOS(TP, FP):
        precisionPos = TP / (TP + FP)
        return precisionPos

    def PrecisionNEG(TN, FN):
        precisionNeg = TN / (TN + FN)
        return precisionNeg

    def RecallPOS(TP, FN):
        recallPos = TP / (TP + FN)
        return recallPos

    def RecallNEG(TN, FP):
        recallNeg = TN / (TN + FP)
        return recallNeg

    def Fmeasure(precision, recall):
        fmeasureValue = (2 * precision * recall) / (precision + recall)
        return fmeasureValue

    def TPR(TP, FN):
        tprValue = TP / (TP + FN)
        return tprValue

    def FPR(FP, TN):
        fprValue = FP / (FP + TN)
        return fprValue

    print("Show Metrics:")

    print("Accuracy: " + str(Accuracy(TP, TN, FP, FN)))

    print("Precision Pos.: " + str(PrecisionPOS(TP, FP)))
    print("Recall Pos.: " + str(RecallPOS(TP, FN)))
    print("Fmeasure Pos.: " + str(Fmeasure(PrecisionPOS(TP, FP), RecallPOS(TP, FN))))

    print("Precision Neg.: " + str(PrecisionNEG(TN, FN)))
    print("Recall Neg.: " + str(RecallNEG(TN, FP)))
    print("Fmeasure Neg.: " + str(Fmeasure(PrecisionNEG(TN, FN), RecallNEG(TN, FP))))

    print("True positive rate: " + str(TPR(TP, FN)))
    print("False positive rate: " + str(FPR(FP, FN)))


def confusion_matrix_measures(matrix, labels=None, beta=1):
    import numpy as np
    import pandas as pd
    matrix = pd.DataFrame(matrix)

    if matrix.shape[0] != matrix.shape[1]:
        print('Wrong matrix! It has different dimensions.')
        return None

    all_predictions = matrix.sum().sum()
    accuracy = np.diag(matrix).sum() / all_predictions
    print('Accuracy={acc:.3f}'.format(acc=accuracy))

    for index in range(len(matrix)):
        precision = matrix.iloc[index, index] / matrix.iloc[:, index].sum()
        recall = matrix.iloc[index, index] / matrix.iloc[index, :].sum()
        fmeasure = (beta * beta + 1) * precision * recall / \
            (beta * beta * precision + recall)
        tp = matrix.iloc[index, index]
        fp = matrix.iloc[:, index].sum() - tp
        fn = matrix.iloc[index, :].sum() - tp
        tn = matrix.iloc[matrix.index != index,
                         matrix.columns != index].sum().sum()
        label = index
        if labels is not None:
            label = labels[index]
        print(
            'Class "{label}": precision={p:.3f}, recall={r:.3f}, f{beta}={f:.3f}, TP={tp}, TN={tn}, FP={fp}, FN={fn}'.format(
                label=label, p=precision, r=recall, beta=beta, f=fmeasure, tp=tp, tn=tn, fp=fp, fn=fn
            ))


# confusion_matrix_measures([[100, 5],[10, 50]])

def calculateDimensions(numbPossibleValues, numbIndValues):
    return print(min(numbPossibleValues-1, numbIndValues))


def AssociationRules(data, minSupport, minThreshold, metric):
    # Todo, before you do this, pop the columns you do not want to use
    from mlxtend.frequent_patterns import association_rules, apriori

    itemsets = apriori(data, min_support=minSupport, use_colnames=True)
    print(itemsets)

    rules = association_rules(
        itemsets, metric=metric, min_threshold=minThreshold)
    print(rules)



# ScatterPlot: Look in file ExamPrep/roby/dataW4/exercise.py


def discriminentAnalysis(subset):
    # Remember to delete NA values from the subset
    subset.dropna(inplace=True)

    # Change these to the columns you have data for
    x = subset[['Length', 'Shoe Size', 'Travel Distance', 'Travel Time']]
    # Change this to the column you need the answer for
    y = subset['Smoker']

    lda = LinearDiscriminantAnalysis()
    lda.fit(x, y)

    # Insert the values of your stuff here
    predictedValue = lda.predict([[173, 40, 5, 10]])
    return print(predictedValue)


def confMatrix(subset):
    # Remember to delete NA values from the subset
    subset.dropna(inplace=True)

    # Change these to the columns you have data for
    x = subset[['Length', 'Shoe Size', 'Travel Distance', 'Travel Time']]
    # Change this to the column you need the answer for
    y = subset['Smoker']

    lda = LinearDiscriminantAnalysis()
    lda.fit(x, y)

    predicted = pd.Series(lda.predict(x), name='predicted')
    actual = subset['Smoker'].rename('actual')
    return pd.crosstab(index=actual, columns=predicted, margins='all', margins_name='total')



def square_conf_matrix(conf_matrix):
    if conf_matrix.index.size > conf_matrix.columns.size:
        names = conf_matrix.index
    else:
        names = conf_matrix.columns
    col_names = names.copy()
    col_names.name = 'predicted'
    row_names = names.copy()
    row_names.name = 'actual'
    data = np.zeros((names.size, names.size), dtype=int)
    new_df = pd.DataFrame(data=data, index=row_names, columns=col_names)
    for j in conf_matrix.columns:
        for i in conf_matrix.index:
            new_df.loc[i][j] = conf_matrix.loc[i][j]
    return new_df

def min_max_norm(col):
    minimum = col.min()
    range = col.max() - minimum
    return (col-minimum)/range


def decimal_scaling_norm(col):
    maximum = col.max()
    tenfold = 1
    while (maximum > tenfold):
        tenfold = tenfold * 10
    return(col/tenfold)


def normalized_values(df, norm_funct):
    df_norm = pd.DataFrame()
    for column in df:
        df_norm[column] = norm_funct(df[column])
    return df_norm


# x_simpsons = normalized_values(x_simpsons, decimal_scaling_norm)

# Metrics for the compile
# Regression = MAE
# Classification = accuracy

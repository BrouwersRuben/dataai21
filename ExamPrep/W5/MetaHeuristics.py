from random import Random
from inspyred import ec  # ec stands for Evolutionary computation
import numpy as np
import pandas as pd
import math
from simanneal import Annealer


"""
Exercise 1 - The backpack (aka the knapsack problem)
You find yourself in a secret room equipped with a door with a time lock. You'll see a timer countdown that 
tells you you only have five minutes left before the door will be locked forever. In front of you are valuable 
objects, each with their own yield and weight. You have a backpack that can carry an absolute maximum weight 
of 750 gr. On Canvas you will find the list of objects with their weight and yield. Put together the optimal 
backpack. You should arrive at an optimal yield of 1458 (or at least a value close to that).
"""

print("\n Exercise 1")

class KnapsackProblem(Annealer):

    def move(self):
        i = np.random.randint(0, len(self.state))
        self.state[i] = np.random.randint(0, 2)

    def energy(self):
        weight = 0
        for i in range(0, len(self.state)):
            weight = weight + items.iloc[self.state[i]]['weight(gr)']
        values = 0
        if weight > 750:
            values = 0
        else:
            for i in range(0, len(self.state)):
                values = values + items.iloc[self.state[i]].value
        return -values


items = pd.read_csv('KnapsackItems.csv', delimiter=',')
init_sol = np.random.randint(0, 2, size=len(items))
rastrigin = KnapsackProblem(init_sol)
res = rastrigin.anneal()

sum = 0
value = 0
for i in range(0, len(res[0])):
    sum += items.iloc[res[0][i]]['weight(gr)']
    value += items.iloc[res[0][i]]['value']
print(sum)
print(value)


def evaluate(candidates, args={}):
    fitness = []
    weights_items = args.get('weights_items', [])
    values_items = args.get('values_items', [])
    for candidate in candidates:
        total_weight = (candidate * weights_items).sum()
        if total_weight > 750:
            total_value = 0
        else:
            total_value = (candidate * values_items).sum()
        fitness.append(total_value)
    return fitness


def generate(random=None, args={}) -> []:
    number_items = args.get('number_items', 15)
    return np.random.randint(0, 2, size=number_items).tolist()


rand = Random()
ga = ec.GA(rand)

population: [ec.Individual] = ga.evolve(
    generator=generate,
    evaluator=evaluate,
    selector=ec.selectors.tournament_selection,  # optional
    pop_size=100,
    maximize=False,
    bounder=ec.Bounder(0, 1),
    max_evaluations=10000,
    mutation_rate=0.25,
    number_items=len(items),
    weights_items=items['weight(gr)'],
    values_items=items['value'])

population.sort(reverse=False)
print(population[0])
res = [1, 1, 0, 0, 1, 1, 0, 1, 0, 0, 1, 0, 1, 1, 0]
(res * items['value']).sum()
(res * items['weight(gr)']).sum()

"""
Exercise 2 - The gutters
You are in charge of the design of gutters where the production cost must be as 
low as possible. That is why it is necessary that the gutters have such an 
optimal cross-section with the available material so that leaves and dirt 
can be easily removed (in other words, the cross-section must be as large 
as possible with the available material.). The company you work for buys 
metal plates that have a width of 1m. So H + B + H -see drawing- must be 
1m or less.Determine the ideal width B and height H of the gutter that 
you can make from the 1m sheets.
"""

print("\n Exercise 2")

class GutterProblem(Annealer):

    def move(self):
        self.state = np.random.uniform(0, 1)

    def energy(self):
        p = (1-2*self.state)*self.state
        return -p


init_sol = np.random.uniform(0, 1)
rastrigin = GutterProblem(init_sol)
res = rastrigin.anneal()


"""
Exercise 3 - The football stadium
The local sports club wants to build a new stadium. The circumference
of the sports field should be 400m, and at the same time we want to 
ensure that the central midfield has a maximum surface area. 
Determine the ideal length and width ratio.
"""

print("\n Exercise 3")


class FootballStadiumProblem(Annealer):
    def move(self):
        # Give all possible option, for example, all lenghts that the football stadium could be
        self.state = np.random.randint(0, 400)

    def energy(self):
        p = 2 * self.state * math.pi
        return -p

init_sol = np.random.randint(0, 400)
rastrigin = FootballStadiumProblem(init_sol)
res = rastrigin.anneal()

def evaluate(candidates, args={}):
    fitness = []
    for candidate in candidates:
        p = 2 * candidate * math.pi
        fitness.append(p)
    return fitness


def generate(random=None, args={}) -> []:
    return np.random.uniform(-1, 1, size=2).tolist()


population: [ec.Individual] = ga.evolve(
    generator=generate,
    evaluator=evaluate,
    selector=ec.selectors.tournament_selection,  # optional
    pop_size=100,
    maximize=False,
    bounder=ec.Bounder(0, 400),
    max_evaluations=100000,
    mutation_rate=0.25, )

population.sort(reverse=False)
print(population[0])

"""
Exercise 4- Optimalization
Given following target function to be maximized:
with following constraints: -1.0 ≤ xi ≤ 1.0 where i=1, 2
Find a good solution.
"""

print("\n Exercise 4")


class OptimisationProblem(Annealer):

    def move(self):
        i = np.random.randint(0, 2)
        self.state[i] = np.random.uniform(-1, 1)

    def energy(self):
        x1 = self.state[0]
        x2 = self.state[1]
        p = 0.2 + x1 ** 2 + x2 ** 2 - 0.1 + \
            math.cos(6 * math.pi * x1) - 0.1 + math.cos(6 * math.pi * x2)
        return -p


init_sol = np.random.uniform(-1, 1, size=2)
rastrigin = OptimisationProblem(init_sol)
res = rastrigin.anneal()


def evaluate(candidates, args={}):
    fitness = []
    for candidate in candidates:
        x1 = candidate[0]
        x2 = candidate[1]
        p = 0.2 + x1 ** 2 + x2 ** 2 - 0.1 + \
            math.cos(6 * math.pi * x1) - 0.1 + math.cos(6 * math.pi * x2)
        fitness.append(p)
    return fitness


def generate(random=None, args={}) -> []:
    return np.random.uniform(-1, 1, size=2).tolist()


population: [ec.Individual] = ga.evolve(
    generator=generate,
    evaluator=evaluate,
    selector=ec.selectors.tournament_selection,  # optional
    pop_size=100,
    maximize=False,
    bounder=ec.Bounder(-1, 1),
    max_evaluations=100000,
    mutation_rate=0.25, )

population.sort(reverse=False)
print(population[0])

class FootballStadiumProblem(Annealer):

    def move(self):
        self.state = np.random.randint(0, 400)

    def energy(self):
        p = 2 * self.state * (200 - self.state) / math.pi
        return -p


init_sol = np.random.randint(0, 400)
rastrigin = FootballStadiumProblem(init_sol)
res = rastrigin.anneal()


def evaluate(candidates, args={}):
    fitness = []
    for candidate in candidates:
        p = 2 * candidate * (200 - candidate) / math.pi
        fitness.append(p)
    return fitness


def generate(random=None, args={}) -> []:
    return np.random.randint(0, 400)


population: [ec.Individual] = ga.evolve(
    generator=generate,
    evaluator=evaluate,
    selector=ec.selectors.tournament_selection,  # optional
    pop_size=100,
    maximize=False,
    bounder=ec.Bounder(0, 400),
    max_evaluations=10000,
    mutation_rate=0.25, )

population.sort(reverse=False)
print(population[0])
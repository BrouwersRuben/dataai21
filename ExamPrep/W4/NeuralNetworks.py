"""
Exercise 1 – The Simpsons Revisited
We want to be able to predict the sex of a Simpson using a neural network (cf. chapter on
Decision trees from Data & A.I. 1.2). Note: this dataset is very small and therefore we
cannot construct a validation set or test set. ...
a.  Read in the Simpsons' dataset from the file ‘The Simpsons original.csv’
b.  What kind of prediction are we trying to make here? Regression or classification?
c.  Determine which columns we can't use in this prediction.
d.  Now create an input training data set x_simpsons and normalize it (min-max
    normalization). Do the same for the output column (y_simpsons).
e.  Create a neural network with the correct inputs and outputs. Make a plot if the ANN.
f.  Use your neural network to predict the gender of your x_simpsons. Do they match
    the targets? Also predict the gender of the unknown Simpson (Comic guy:
    hairlenght=8, weight=500, age=38). Does this match the prediction you made with
    the decision trees (see Data & AI 1.2)?
"""

import numpy as np
import pandas as pd
from tensorflow import keras
from tensorflow.keras import Model
from tensorflow.keras.layers import Input, Dense, BatchNormalization
from tensorflow.keras.utils import to_categorical
from tensorflow. keras.optimizers import Adam
from livelossplot import PlotLossesKeras
from keras.utils.vis_utils import plot_model


# A
simpsons = pd.read_csv('TheSimpsonsOriginal.csv', delimiter=',')
print(simpsons)

# B
# Classification

# C
# Age & Name

# D

x_simpsons = simpsons[['hair length', 'weight', 'age']]
y_simpsons = simpsons['gender']


def min_max_norm(col):
    minimum = col.min()
    range = col.max() - minimum
    return (col-minimum)/range


def normalized_values(df, norm_funct):
    df_norm = pd.DataFrame()
    for column in df:
        df_norm[column] = norm_funct(df[column])
    return df_norm


x_simpsons = normalized_values(x_simpsons, min_max_norm)

y_simpsons = y_simpsons.replace('M', 1)
y_simpsons = y_simpsons.replace('F', 2)

# E
inputs_xor = Input(shape=(3,))
x_xor = Dense(3, activation='sigmoid')(inputs_xor)
x_xor = Dense(3, activation='sigmoid')(x_xor)
outputs_xor = Dense(1, activation='softmax')(x_xor)
model_xor = Model(inputs_xor, outputs_xor, name='XOR_NN')

model_xor.compile(optimizer=Adam(learning_rate=0.00001),
                  loss=keras.losses.binary_crossentropy, metrics=['accuracy'])

#callbacks=[PlotLossesKeras()]
history_xor = model_xor.fit(x_simpsons, y_simpsons, epochs=200, verbose=False)

model_xor.summary()
plot_model(model_xor, to_file='model_xor_plot.png',
           show_shapes=True, show_layer_names=True)

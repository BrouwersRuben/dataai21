"""
The director of KdG claims that the average study time per course is 4.5 hours per
week in the programme. To substantiate this, a survey was conducted among the
students. The results of the students who have actually completed the questionnaire
can be found in the csv file: ‘ studyResult.csv’ csv’. The column ‘study time’ contains the
hours per week the students spend on the course ‘Data & AI’
a) What alternative hypothesis can we use here
b) What is the confidence interval for the study time of the programme at α=0.05?
   Also write down how you came to this interval
c) What factor did you use to calculate the confidence interval?
d) Which statement is correct regarding the answer in question b.
    a. I reject the director’s claim. There is a 5 % chance that I will make a Type II mistake.
    b. I do not reject the claim because I am not sure enough given the value of α.
    c. I can only be 5 % sure that the director’s claim is correct
    d. None of the above statements is correct
    A teacher claims that the average study duration for Data & AI is greater than 4.0.
e) Calculate the p value you can use to substantiate this statement(at α=0.05)
f) Based on the sample, what can you say about the teacher's claim?
"""

# Data given:
#     Claim: Study time per course with µ = 4.5
#     sample: study time for the course Data & AI
#     Claim: study time Data & AI is equal to the study time of a course in the programme (= 4.5)
#     H0: µ=4.5
#     H1: µ ≠ 4.5

import pandas as pd
import math
from scipy.stats import t  # t-test
from scipy.stats import stats

# A
# H 1: µ ≠ 4.5

# B
study = pd.read_csv('studyResult.csv', decimal=',', delimiter=';')
n = len(study)
s = study['study time'].std()/math.sqrt(395)
print(t.interval(alpha=0.95, df=n-1, loc=4.5, scale=s))

# C
print(t.ppf(0.975, n-1))

# D
mu = study['study time'].mean()
print(mu)

# E
print(stats.ttest_1samp(a=study['study time'], popmean=4.0, alternative='greater'))
# Answer: 
# H0: µ = 4.0
# H1: µ > 4.0

# F
# p value > α --> H 0 hypothesis(smaller or equal) can not be discarded.
# We can not go along with the teacher’s claim.

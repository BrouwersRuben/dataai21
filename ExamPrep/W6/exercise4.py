"""
Use dataframe AdultUCI.csv.
a. Delete the columns with numerical data
b. Converts the data frame to a transactions object
c. Apply the apriori algorithm with the following parameters: min_support = 0.05, min_confidence = 0.6, min_length = 2, max_length = 3.
   How many item sets did you get?
d. Select the item sets with a support larger than 0.5 and sort these item sets
   (highest support first). How many are
   """
from mlxtend.frequent_patterns import apriori
import pandas as pd

adultUCI = pd.read_csv('AdultUCI.csv', delimiter=';')
# adultUCI.info()

# A 
adultUCI = adultUCI.drop(columns=['age', 'fnlwgt', 'education-num', 'capital-gain', 'capital-loss', 'hours-per-week'])

# B
transactions = pd.get_dummies(adultUCI, prefix_sep='=')
# transactions.head()

# C
itemsets = apriori(transactions, min_support=0.05, max_len=3, use_colnames=True)
print(itemsets)

# D
items_support = itemsets[itemsets['support'] >= 0.5]
items_support.sort_values(by='support', ascending=False)
print(items_support.count())

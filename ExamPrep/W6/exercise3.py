"""
We want to find out whether the partying
behaviour(again use the dataset
          studyResult.csv) of the students of our
programme differs from that of all students of
KdG. A survey of all KdG students shows the
following distribution
a) Calculate  x²
b) What is the p value for this distribution?
c) I want to make a decision with a 90 % reliability. What decision do I make?
"""

import pandas as pd
from scipy.stats import chisquare
import math

study = pd.read_csv('studyResult.csv', decimal=',', delimiter=';')

study.partying = study.partying.astype(pd.CategoricalDtype(categories=['Never','Rarely', 'Once a week', 'Several times a week', 'Every day']))
measured_values=study.partying.value_counts(dropna=False).sort_index()
print(measured_values)
n = len(study)

# A
# if x² large -> H0 probably wrong, if x² small -> H0 probably right
# x = math.sqrt(recordedAmount - expectedAmount) / expectedAmount + ...
x = (23 - (0.05*n))**2 / (0.05*n) + (103 - (0.3*n))**2 / (0.3*n) + (130 - (0.35*n))**2 / (0.35*n) + (86 - (0.2*n))**2 / (0.2*n) + (53 - (0.1*n))**2 / (0.1*n)
print(x)

# B
# What has been claimed
expected_values = [0.05*n, 0.3*n, 0.35*n, 0.2*n, 0.1*n]
print(chisquare(measured_values, expected_values))

# C
# P-value = 0.08 = 8%
# error = 10%
# p-value < acceptance region -> H0 must be rejected

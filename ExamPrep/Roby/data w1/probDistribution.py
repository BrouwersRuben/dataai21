from scipy.stats import binom
from scipy.stats import poisson
from scipy.stats import norm
import scipy.stats as st

"""
Exercise 1
Luna rolls a die 4 times (answer with 4 significant digits):
a. What is the probability of exactly one 6?
b. What is the probability of four sixes?
c. What is the probability of 2 throws under the 3 and 2 throws 3 or higher?
"""
#binom.pmf(x,n,p)
print('Exercise 1')
#binomial distribution --> 6 or no 6
#chance that you have EXACTLY one 6 (1, 4 tries, 1/6 chance for it to be a 6) 
print('1a: ' + str(binom.pmf(1,4,1/6)))
#chance that you have EXACTLY one 6(4, 4 tries, 1/6 chance for a 6)
print('1b: ' + str(binom.pmf(4,4,1/6)))
#chance that you have 2 throws under 3 (2/6) AND 2 throws above 3 (can be left out because it's automatically above 3)
print('1c: ' + str(binom.pmf(2,4,2/6)))

"""
Exercise 2
Six students of Applied Computer Science must take an English test. The chance to pass the
exam is 0.75.
a. What are the chances that exactly 4 of them will succeed?
b. What are the chances that exactly 5 of them will succeed?
c. What are the chances that exactly 6 of them will succeed?
d. What are the chances that less than 4 of them will succeed?
"""
print('Exercise 2')
#binomial distribution again --> succeed or no succeed
#chance that EXACTLY 4 succeed
print('2a: ' + str(binom.pmf(4, 6, 0.75)))
#chance that EXACTLY 5 succeed
print('2b: ' + str(binom.pmf(5, 6, 0.75)))
#chance that EXACTLY 6 succeed
print('2c: ' + str(binom.pmf(6, 6, 0.75)))
#chance for less than 4
#also works
#binom.pmf(1, 6, 0.75) + binom.pmf(2, 6, 0.75) + binom.pmf(3, 6, 0.75)
print('2d: ' + str(binom.cdf(3,6,0.75)))

"""
Exercise 5
A user receives an average of 20 emails per day. One day he gets 100 and wonders if this is
still normal.
a. What is the probability that he will receive more than 100 emails in a day?
b. what is the probability that he will get more than 30 in a day?
c. what is the probability that he will get exactly 20 in a day?
d. What is the probability that he will get 10 or less in a day?
e. what is the probability that he will get 650 or less in a month (30 days)?
"""

print('Exercise 5')
#Poisson distribution --> indicates how many times certain events occur in a time interval
#more than 100
print('5a: ' + str(1 - poisson.cdf(100, 20))) #--> extremely low (0)
#more than 30
print('5b: ' + str(1 - poisson.cdf(30, 20)))
#exactly 20
print('5c: ' + str(poisson.pmf(20, 20)))
#10 or less
print('5d: ' + str(poisson.cdf(10, 20)))
#650 or less in 30 days (20*30)
print('5e: ' + str(poisson.cdf(650, 20*30)))

"""
Exercise 7 (multiple choise question)
A test consists of 40 questions and the average difficulty of the questions is 0.85 (the probability of a correct answer is 0.85). The students receive 1 point per Exercise. What are the expected value and the standard deviation of the score on this test?
a. µ=34 and ơ=2.26
b. µ=29 and ơ=2.26
c. µ=34 and ơ=5.10
d. µ=29 and ơ=5.10
"""
#µ 40*0.85 = 34 --> b and d false
#binom.std(amount, chance)
#print(binom.std(40,0.85)) --> 2.2583179581272432
#option a is correct

"""
Exercise 8
Consider the standard normal distribution. What are the probabilities for the following situations:
1. P(Z ≥ +1.64)
2. P(Z ≥ -1.32)
3. P(Z ≤ -0.18)
4. P(Z ≤ +1.28)
5. P(0.45 ≤ Z ≤ 0.92)
6. P(-0.72 ≤ Z ≤ -0.38)
"""

#≥ = norm.cdf
#≤ = norm.sf
print('Exercise 8')
print('8.1: ' + str(1 - norm.cdf(1.64)))
print('8.2: ' + str(norm.cdf(1.32)))
print('8.3: ' + str(norm.sf(0.18)))
print('8.4: ' + str(1 - norm.sf(1.28)))
print('8.5: ' + str(abs(((1 - norm.sf(0.45)) - (1 - norm.sf(0.92))))))
print('8.6: ' + str(abs((norm.sf(0.72) - norm.sf(0.38)))))

"""
Exercise 9
In a class, the average hair length is normally divided μ=20 cm and ơ=4. What are the probabilities for the following situations:
a. The chance that someone's hair is longer than 28cm
b. The chance that someone's hair is shorter than 16cm
c. The chance that someone's hair has a length between 18cm and 22cm
"""

print('Exercise 9')
#a. The chance that someone's hair is longer than 28cm
#Longer --> 1-norm.cdf(...)
print('Exercise 9a: ' + str(1 - norm.cdf(28, loc=20, scale=4)))
#b. The chance that someone's hair is shorter than 16cm
#Shorter --> norm.cdf(...)
print('Exercise 9b: ' + str(norm.cdf(16, loc=20, scale=4)))
#c. The chance that someone's hair has a length between 18cm and 22cm
print('Exercise 9c: ' + str(abs((norm.cdf(18, loc=20, scale=4) - (norm.cdf(22, loc=20, scale=4))))))

"""
Exercise 11
In an IQ test, the expected value is 100 (normally distributed). In Antwerp the standard
deviation is 15, in Ghent it is 18.
a. What is the chance that someone in Antwerp has an IQ that is greater than 120?
b. With what IQ do you belong in Antwerp to the people who have the 16% lowest scores?
Hint: experiment until you find it
c. What IQ (or more) do the 5% smartest people in Ghent have?
Hint: experiment until you find it
d. What is the ratio of Antwerp and Ghent residents for scores above 130 (multiple choise
question)?
1. 1:1
2. 1:2
3. 1:3
4. 5:6
"""

print('Exercise 11')

#a. What is the chance that someone in Antwerp has an IQ that is greater than 120?
print('11a: ' + str(1 - norm.cdf(120, loc=100, scale=15)))
#b. With what IQ do you belong in Antwerp to the people who have the 16% lowest scores?
#Hint: experiment until you find it
#seriously???
print('11b: ' + str(norm.cdf(85, loc=100, scale=15)))
#c. What IQ (or more) do the 5% smartest people in Ghent have?
#Hint: experiment until you find it
#????
print('11c: ' + str(1 - norm.cdf(129.6, loc=100, scale=15)))
#d. What is the ratio of Antwerp and Ghent residents for scores above 130 (multiple choise question)?
#print('Antwerp: ' + str(1 - norm.cdf(130, loc=100, scale=15))) 0.02275013194817921
#print('Ghent: ' + str(1 - norm.cdf(130, loc=100, scale=18))) 0.047790352272814696
#option 2: 1:2 

"""
Exercise 14
A new machine makes fully autonomous processors. Only 2 out of 5 processors meet the
quality standard.
a. We take 10 processors made by this machine. What is the probability that less than 2
are good?
b. We take 100 processors made by this machine. What are the chances that less than 20
are good?
c. Both of the above questions ask for less than 20% of the total number of processors.
What difference do you notice? What could be the cause?

"""

print('Exercise 14')

#a. We take 10 processors made by this machine. What is the probability that less than 2 are good?
#Less than 2 = 1
print('14a: ' + str(binom.cdf(1, 10, 2/5)))
#b. We take 100 processors made by this machine. What are the chances that less than 20 are good?
print('14b: ' + str(binom.cdf(19, 100, 2/5)))
#c. Both of the above questions ask for less than 20% of the total number of processors.
#What difference do you notice? What could be the cause?
#In the first scenario, we encountered 2 less than we expected ((2/5) * 10 = 4)
#In the second scenario, we encountered 20 less than we expected ((2/5) * 100 = 40)
#This equals a much more extreme situation. This is an example of the higher the sample size, the more accurate your calculations

"""
Exercise 15
During a laptop exam, anything can go wrong.
We know that a computer running Windows has a 2% chance of crashing during the exam.
For Mac OSX it is 0.2% and for Linux it is 0.1%.
Let's say a class of 40 students comes to take the exam.
Situation 1: everyone is running Windows:
a. What is the expected number of crashes during the exam?
b. How much chance is there that 1 computer crashes during the exam?
c. How much chance is there that 2 computers crash during the exam?
Situation 2: 10 students run Windows, 10 students run Mac OSX and 20 run Linux
a. What is the expected number of crashes during the exam?
b. How much chance is there that 1 computer crashes during the exam?
c. How much chance is there that 2 computers crash during the exam?

"""

print('Exercise 15')
#Situation 1: everybody uses Windows
print('Situation 1')
#a. What is the expected number of crashes during the exam?
#40 students --> 2% chance of them crashing when using windows, in this case everybody uses it
#40 * 0.02 = 0.8

#b. How much chance is there that 1 computer crashes during the exam?
print('15.1b: ' + str(binom.pmf(1, 40, 0.02)))

#c. How much chance is there that 2 computers crash during the exam?
print('15.1c: ' + str(binom.pmf(2, 40, 0.02)))

#Situation 2: 10 windows, 10 mac, 20 linux
print('Situation 2')
#a. What is the expected number of crashes during the exam?
# 10 * 0.02 + 10 * 0.002 + 20 * 0.001
#print(10 * 0.02 + 10 * 0.002 + 20 * 0.001) 
# 0.24

#don't know
#not correct
#print('15.2b: ' + str(binom.pmf(1, 10, 0.02) + binom.pmf(1, 10, 0.002) + binom.pmf(1, 20, 0.001)))

"""
Exercise 17
An IT service desk received an average of 10 service requests per hour.
a. Calculate the probability that more than 15 service requests will be received in an hour.
b. Calculate the probability that at least 10 but no more than 15 service requests will be
received.
c. Calculate the chance that no service requests will be received for fifteen minutes.
"""

print('17a:' + str(1 - poisson.cdf(15, 10)))
print('17b: ' + str((1 - poisson.cdf(9, 10)) - (1 - poisson.cdf(15, 10))))
print('17c: ' + str((poisson.pmf(0, 2.5))))
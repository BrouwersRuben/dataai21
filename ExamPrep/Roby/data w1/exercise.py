"""
Exercise 1
You roll with 2 dice (each dice is 6-sided). What are the chances that you will throw a 5 and a
2? (3 significant figures)

"""
# Probability of a 2 = 1/6
# Probability of a 5 = 1/6
# So: prob 2 & 5 = 1/6 * 1/6

# Probability of a 5 = 1/6
# Probability of a 2 = 1/6
# So: prob 5 & 2 = 1/6 * 1/6

# So together 1/6 * 1/6 + 1/6 * 1/6

probEx1 = 1/6 * 1/6 + 1/6 * 1/6
print(probEx1)
"""
Exercise 2
We have 2 dice.
1st: 3 yellow areas and 3 red areas
2nd: 5 yellow areas and 1 red area.
What are the chances of both a yellow and a red area?

"""
# Probability of a red area dice 1: 3/6
# Probability of a yellow area dice 2: 5/6
# So: 3/6 * 5/6

# Probability of a yellow area dice 1: 3/6
# Probability of a red area dice 2: 1/6
# So: 3/6 * 1/6

probEx2 = 3/6 * 5/6 + 3/6 * 1/6
print(probEx2)
"""
Exercise 3
One box contains 4 yellow, 2 brown, 1 orange, 1 green and 2 purple marbles. For each of the
following situations we start with all the marbles in the box. While pulling the marbles, we will
not return the pulled marble(s) back in the box. (2 significant figures each):
a. When we pull 4 marbles, what are the chances that all the marbles are yellow?
b. When we pull 2 marbles, what is the probability of a brown and a green marble?
c. When we pull 1 marble, what is the probability of an orange or a green marble?

"""
yellow = 4
brown = 2
orange = 1
green = 1
purple = 2
maxMarbels = yellow + brown + orange + green + purple

aPulledMarbels = 4
bPulledMarbels = 2
cPulledMarbels = 1

# A When we pull 4 marbles, what are the chances that all the marbles are yellow?
# Every time a marble is pulled, they don't go back into the box
# After each consecutive pull, the amount of balls in the box reduces by 1 (because you take one out)
firstBallYellow = 4 / 10
secondBallYellow = 3 / 9
thirdBallYellow = 2 / 8
fourthBallYellow = 1 / 7

probex3A = firstBallYellow * secondBallYellow * thirdBallYellow * fourthBallYellow
print("3A : " + str(probex3A))

# B When we pull 2 marbles, what is the probability of a brown and a green marble?
# Prob brown all balls = 2/10
# Prob green all balls 1/10

# first brown then green
#2 situations
firstPull = 2/10 * 1/9
secondPull = 1/10 * 2 /9

probex3B = firstPull + secondPull
print("3B: " + str(probex3B))

# C When we pull 1 marble, what is the probability of an orange or a green marble?
#keyword OR --> add
# Prob green all balls 1/10
# Prob orange all balls 1/10

firstPull = 1/10
secondPull = 1/10
probex3C = firstPull + secondPull
print("3C: " + str(probex3C))

"""
Exercise 5
Out of a group of 1000 students, there are 400 who regularly play computer games (C) and
200 who regularly play board games (B). There are 100 who regularly play computer games
and board games (C and B).
What are the chances that if you choose a random student that they regularly play games
(board or computer)?
"""

#Subtract the third group (board and computer games), else this group will be counted twice (they're already counted inside group 1 and 2)
print('Exercise 5: ' + str(400/1000 + 200/1000 - 100/1000))

"""
Exercise 6
For a Data & AI exam, 70% of the students pass. Of these, 50% are day students and 50%
are evening students. Of the participants, 60% are day students and 40% are evening
students. From the group, 1 random student is selected each time. (2 significant figures each)
a. What is the chance of a successful student?
b. What is the probability of a day student, given that he is a successful student?
c. What is the chance of a successful day student?
d. What is the chance of a successful student, given that he is a day student?
e. What is the probability of a failed student, given that he is a day student?
f. What is the chance of a failed day student?
"""

#a. What is the chance of a successful student?
#Answer is inside text (70% pass)
print('6a: 0.7')

#b. What is the probability of a day student, given that he is a successful student?
#Answer is also inside the text (50% that it's a day student if they pass)
print('6b: 0.5')

#c. What is the chance of a successful day student?
#Question on entire population, so take the 70% pass chance and multiply by the 50% pass chance that it's a day student
print('6c: ' + str(0.7*0.5))

#d. What is the chance of a successful student, given that he is a day student?
#Question on entire population, so take the 70% into account
#Out of the entire population, 60% are day students. 70% of that number passes and the probability of somebody passing and being a day student is 50%
print('6d: ' + str(0.7/0.6 * 0.5))

#e. What is the probability of a failed student, given that he is a day student?
#This is the reverse answer of d. --> 1 - probability of passing
print('6e: ' + str(1 - (0.7/0.6 * 0.5)))

#f. What is the chance of a failed day student?
#Question on the entire population --> first check if student is a day student (60%) and then see the probability of the student failing as a day student (question e)
print('6f: ' + str(0.6 * (1 - (0.7/0.6 * 0.5))))

"""
Exercise 7
3 men go to a party and wear the exact same coats. They hang their coats on the coat rack
and then get very drunk.
At the end, each man takes a coat without looking at which one. What are the chances that
everyone will take their own coat?
"""
#Same as exercise 3, when the first person takes their correct coat, only 2 options remain for the second person (1 correct coat and then 1 false coat). When the last person comes around there is only one remaining option (the correct coat)
print('Exercise 7: ' + str(1/3*1/2*1/1))

"""

Exercise 8
If it rains hard, there is a 50% chance that my basement will flood. It rains on average 35
days / year hard and my basement is flooded 20 days a year (so sometimes even when it does
not rain hard).
If my basement is flooded, what is the chance that this is because it rains hard?
"""

#Also viable
#print((35 / 20) * 0.5)

print('Exercise 8: ' + str(0.5 * (35/365) / (20/365)))

"""
Exercise 9
90% of PCs use Windows. If a PC crashes, you are 99.9% sure that it is running Windows on
it. The chance that a PC crashes (regardless of the operating system) is 0.01.
What is the chance that a PC will crash if you know that windows is running on it?

"""
#the chance of crashing (when windows) * the chance that the chance a pc crashes (regardless to OS) divided by the percentage of windows users
print('Exercise 9: ' + str((0.999 * 0.01) / 0.9))

"""
Exercise 10
In 1988 there was discussion to introduce a mandatory AIDS test. A very reliable test has
been developed which achieves the following results:
P(positive | infected) = 0.999
P(negative | not infected) = 0.99 --> p(positive | not infected) = 1 - 0.99 = 0.01
It is estimated that 0.6% of people are carriers of the virus.
What is the chance that someone is actually infected if you know that the test is positive? Do
you see why the mandatory test was not introduced?

"""

#Check the chance for an infected person with a positive test (there can be false positives or false negatives)
#Positive test | infected (0.999) * p(infected) (0.6% --> 0.006) / p(positive) --> calculate first
#you already have: 0.999 * 0.006/???

#p(positive) = p((infected and positive) or (not infected and positive))
#0.994 = chance that you're not infected (1 - 0.006 --> infected chance)
#0.006 * 0.999 + 0.994 * 0.01
pPositive = 0.006 * 0.999 + 0.994 * 0.01
print('Exercise 10: ' + str(0.999 * 0.006/pPositive))

"""
Exercise 11
A program was written that determines when a text is interesting for a reader. The program
has already been trained and the following values have been noted:
1642 texts were analysed by hand. 46 of the texts were referred to as interesting. A folder
was made of words and the number of times they appeared as well as the number of times
they appeared in an interesting text.
We now get the following text: "This is a text to see if it will be good enough to be interesting
reading."
The folder contains the following values for these words:
word frequency freq in interesting texts
this 1368 40
is 1642 46
a 1642 46
text 159 1
to 1480 24
see 1574 38
if 947 11
it 1642 35
will 1608 46
be 1576 39
good 156 2
enough 1387 46
interesting 1589 14
reading 176 1
What is the probability that this text is an interesting text, according to the algorithm seen? Is
this chance big or small?
"""
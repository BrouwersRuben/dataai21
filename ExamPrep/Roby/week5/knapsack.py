import pandas as pd
import numpy as np
#from clang.cindex import xrange

items = pd.read_csv('Knapsack Items.csv', delimiter=',')


def knapsack(capacity, weights, gain, n):
    if n == 0:
        return 0
    if weights[n - 1] > capacity:
        return knapsack(capacity, weights, gain, n - 1)
    else:
        return max(gain[n - 1] + knapsack(capacity - weights[n - 1], weights, gain, n - 1),
                   knapsack(capacity, weights, gain, n - 1))


def greedy_algo(items, capacity):
    subset = items[['item number', 'weight(gr)', 'value']]
    items = [tuple(x) for x in subset.to_numpy()]
    items = sorted(items, key=lambda item: item[2], reverse=True)
    chosen = {}  # iterated items will be added to this dictionary
    profit = 0
    for i in range(len(items)):
        name, value, weight = items[i]
        num_of_items = (capacity - (capacity % weight)) / weight
        chosen[name] = num_of_items
        capacity = capacity % weight
        profit += num_of_items * value
    return round(profit, 2), chosen


def greedy_algo_dantzig(items, capacity):
    subset = items[['item number', 'weight(gr)', 'value']]
    items = [tuple(x) for x in subset.to_numpy()]
    items = sorted(items, key=lambda item: item[2] / item[1], reverse=True)
    chosen = {}  # iterated items will be added to this dictionary
    profit = 0
    for i in range(len(items)):
        name, value, weight = items[i]
        num_of_items = (capacity - capacity % weight) / weight
        chosen[name] = num_of_items
        capacity = capacity % weight
        profit += num_of_items * value
    return round(profit, 2), chosen


def dynamic_knapsack(items, capacity):
    subset = items[['item number', 'weight(gr)', 'value']]
    items = [tuple(x) for x in subset.to_numpy()]
    bag = [0 for i in range(capacity + 1)]
    for i in range(capacity + 1):
        for j in range(len(items)):
            _, value, weight = items[j]
            if weight < i:
                bag[i] = max(bag[i], bag[i - weight] + value)
    return round(bag[capacity])


"""
* THE MOST EFFECTIVE FUNCTION *
------------------------------------------------
knapsack_dp(values,weights,n_items,capacity,return_all=False)
Input arguments:
  1. values: a list of numbers in either int or float, specifying the values of items
  2. weights: a list of int numbers specifying weights of items
  3. n_items: an int number indicating number of items
  4. capacity: an int number indicating the knapsack capacity
  5. return_all: whether return all info, defaulty is False (optional)
Return:
  1. picks: a list of numbers storing the positions of selected items
  2. max_val: maximum value (optional)
------------------------------------------------
"""
def knapsack_dp(values, weights, n_items, capacity, return_all=False):
    table = np.zeros((n_items + 1, capacity + 1), dtype=np.float32)
    keep = np.zeros((n_items + 1, capacity + 1), dtype=np.float32)

    for i in range(1, n_items + 1):
        for w in range(0, capacity + 1):
            wi = weights[i - 1]  # weight of current item
            vi = values[i - 1]  # value of current item
            if (wi <= w) and (vi + table[i - 1, w - wi] > table[i - 1, w]):
                table[i, w] = vi + table[i - 1, w - wi]
                keep[i, w] = 1
            else:
                table[i, w] = table[i - 1, w]

    picks = []
    K = capacity

    for i in range(n_items, 0, -1):
        if keep[i, K] == 1:
            picks.append(i)
            K -= weights[i - 1]

    picks.sort()
    picks = [x - 1 for x in picks]  # change to 0-index

    if return_all:
        max_val = table[n_items, capacity]
        return picks, max_val
    return picks

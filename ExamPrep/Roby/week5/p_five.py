import math
import numpy as np
import pandas as pd
import tsp
from python_tsp.exact import solve_tsp_dynamic_programming
from simanneal import Annealer

"""
# |-------------------------------------------------------|
# |*******************************************************|
# | < < < <    M E T A  -  H E U R I S T I C S    > > > > |
# |*******************************************************|
# |-------------------------------------------------------|
"""
"""
# #####################################################
# < < <               P A R T  -  1               > > >
# #####################################################
"""

""" << Optimization Problems >> """
distance_matrix = np.array([[0, 100, 125, 100, 75], [100, 0, 50, 75, 100],
                            [125, 50, 0, 100, 125], [100, 75, 100, 0, 50],
                            [75, 100, 125, 50, 0]])

# 1st approach ~ tsp
r = range(len(distance_matrix))
# dictionary of distances
shortest_way = {(i, j): distance_matrix[i][j] for i in r for j in r}
print(tsp.tsp(r, shortest_way))

# 2nd approach ~ python-tsp
permutation, distance = solve_tsp_dynamic_programming(distance_matrix)
print((permutation, distance))

""" SIMULATED ANNEALING """
class RastriginProblem(Annealer):
    """Test Annealer from package simanneal with the Rastrigin"""

    def move(self):
        i = np.random.randint(0, len(self.state))
        self.state[i] = np.random.uniform(5.12, 5.12)

    def energy(self):
        sum = 10 * len(self.state)
        for i in range(0, len(self.state)):
            sum = sum + (self.state[i] ** 2 - 10 * math.cos(2 * math.pi * self.state[i]))
        return sum


init_sol = np.random.uniform(-5.12, 5.12, size=30)  # initial solution
rastrigin = RastriginProblem(init_sol)
# set annealing parameters, if not default values will be used
rastrigin.anneal()
print(rastrigin.Tmin)  # 2.5
print(rastrigin.Tmax)  # 25000.0


""" EXERCISE ON SIMULATED ANNEALING """
class ExerciseProblem(Annealer):
    def move(self):
        rand_fact = np.random.uniform(-1, 1)
        i = np.random.randint(0, 2)
        self.state[i] = np.random.uniform(-20, 20)
        if self.state[i] < -20:
            self.state[i] = -20
        if self.state[i] > 20:
            self.state[i] = 20

    def move_cutsom(self, upper_limit, lower_limit, start, end):
        rand_fact = np.random.uniform(-1, 1)
        i = np.random.randint(start, end)
        self.state[i] = np.random.uniform(lower_limit, upper_limit)
        if self.state[i] < lower_limit:
            self.state[i] = lower_limit
        if self.state[i] > upper_limit:
            self.state[i] = upper_limit

    def energy(self):
        fact1 = self.state[0]
        fact2 = self.state[1] + 9
        return -(fact1 * math.sin(math.sqrt(abs(fact1 - fact2))) - fact2 * math.sin(math.sqrt(abs(fact2 + fact1 / 2))))



# (array([19.99697527,  8.71860021]), -35.043411588376266)

"""
# #####################################################
# < < <               P A R T  -  2               > > >
# #####################################################
"""

""" TABU SEARCH """
def objective_function_V1(solution, weights):  # VERSION 1 check also the constraints
    n = int(math.sqrt(len(solution)))
    # leave each city once
    leaveOK = np.empty((n, 1), dtype=int)
    for i in range(0, n):
        index = range(i, n * n, n)
        leaveOK[i] = 0
        for j in index:leaveOK[i] = leaveOK[i] + solution[j]
    # Arrive in each city once
    arriveOK = np.empty((n, 1), dtype=int)
    for i in range(0, n):
        index = range(i * n, (i + 1) * n, 1)
        arriveOK[i] = 0
        for j in index:arriveOK[i] = arriveOK[i] + solution[j]
    # Never stay in a city
    index = range(0, n*n, n+1)
    notStayingOK = 0
    for j in index: notStayingOK = notStayingOK + solution[j]
    # No sub-loops or infinite loop, but one loop with length n
    loop_length = 0
    city = 0
    in_loop = True
    while(in_loop & (loop_length < n+1)):
        loop_length = loop_length + 1
        index = range(city*n, (city+1)*n, 1)  # row of city
        next_city = 0
        while ((solution[index[next_city]] == 0) & (next_city < n - 1)): next_city=next_city+1
        in_loop = (next_city != 0) & (solution[index[next_city]] == 1)
        city = next_city
    # Test if all 4 of the conditions are fulfilled
    if ((notStayingOK == 0) & (np.min(arriveOK) == 1) & (np.max(arriveOK) == 1) & (np.sum(arriveOK) == n)
        & (np.min(leaveOK) == 1) & (np.max(leaveOK) == 1) & (np.sum(leaveOK) == n) & (loop_length == n)):
        score=np.sum(np.multiply(solution, weights))  # value objective function
    else:
        score=1000*n  # not a feasible solution, so very bad value for the objective function
    return score


def objective_function_V2(solution, weights):  # VERSION 2 no check of the constraints
    return np.sum(np.multiply(solution, weights))


def neighbourhood_V1B(solution):  # VERSION 1 A 1 random solution no constraints met
    n = int(len(solution))
    solution = np.zeros(n, dtype=int)
    for i in range(0, n):
        solution[i] = np.random.randint(low=0, high=2)
    return solution.tolist()


def neighbourhood_V1A(solution):  # VERSION 1 B 1 random solution one constraints met
    num_cities = int(math.sqrt(len(solution)))
    solution = np.zeros(num_cities * num_cities,dtype=int)
    for i in range(0, num_cities):
        j = np.random.randint(low=0, high=num_cities)
        solution[i * num_cities + j]=1
    return solution.tolist()


def neighourhood_V2(solution):  # VERSION 2 all solutions where 2 two cities were swapped positions
    neigh_list = []
    n=int(math.sqrt(len(solution)))
    for j in range(0,n-1): # j: index of first city to swap
        for p in range(j+1,n): # p: index of second city to swap
            if j != p:
                neigh_sol = solution.copy()
                # column of j to find i: the index of the city before j in the loop
                index = range(j,n*n, n)
                i = 0
                while neigh_sol[index[i]] == 0: i=i+1
                # row of j to find k: the index of the city after j in the loop
                index = range(j*n,(j+1)*n, 1)
                k = 0
                while neigh_sol[index[k]] == 0: k=k+1
                # column of p to find o: the index of the city before p in the loop
                index = range(p,n*n, n)
                o = 0
                while neigh_sol[index[o]] == 0: o=o+1
                # row of p to find q: the index of the city after p in the loop
                index = range(p*n,(p+1)*n, 1)
                q = 0
                while neigh_sol[index[q]] == 0: q=q+1
                # make the swap in the loop take care of situations where j and p are next to each other
                neigh_sol[i*n+j] = 0  # i --> j
                neigh_sol[j*n+k] = 0  # j --> k
                neigh_sol[o*n+p] = 0  # o --> p
                neigh_sol[p*n+q] = 0  # p --> q
                if i != p : neigh_sol[i*n+p] = 1  # i --> p
                if j != q : neigh_sol[j*n+q] = 1  # j --> q
                if o != j : neigh_sol[o*n+j] = 1  # o --> j
                if p != k : neigh_sol[p*n+k] = 1  # p --> k
                if ((p == k) & (j == o)): neigh_sol[p*n+j] = 1  # p --> j
                if ((p == i) & (j == q)): neigh_sol[j*n+p] = 1  # j --> p
                neigh_list.append(neigh_sol)
    return neigh_list


""" GENETIC ALGORITHM """

""" - IMPORTANT: TERMINOLOGY -
Gene -> property of a
Chromosome -> set of genes
Individual -> is determined by its chromosome
Population -> set of individuals
Generations -> a sequence of populations
Mating -> emergence of a new individual (child) from 2 old individuals (parents)
by cross over of their chromosomes.
Cross over -> take a portion of the chromosomes of one parent, the remaining
portion of the other parent, and combining them to form a child.
Mutation -> random gene change in an individual's chromosome.
Fitness function -> function that determines the suitability of the individuals
Selection mechanism -> determines which individuals are allowed to mate
"""

""" << GENETIC ALGORITHMS >> """
def obj_func(solution):
    sum = 10 * len(solution)
    for i in range(0, len(solution)):
        sum = sum+(solution[i]**2-10*math.cos(2*math.pi*solution[i]))
    return sum

def generate(random=None, args=None) -> []:
    size = args.get('num_inputs', 10)
    return np.random.uniform(low=-5.12, high=5.12, size=size)

def evaluate(candidates, args={}):
    fitness = []
    for candidate in candidates:
        fitness.append(obj_func(candidate))
    return fitness


""" RASTRIGIN FUNCTION """
import inspyred
from inspyred import ec  # ec stands for Evolutionary computation
from random import Random
rand = Random()
ga = ec.GA(rand)
population: [ec.Individual] = ga.evolve(
    generator=generate,
    evaluator=evaluate,
    selector=ec.selectors.tournament_selection, #optional
    pop_size=100,
    maximize=False,
    bounder=ec.Bounder(-5.12, 5.12),
    max_evaluations=10000,
    mutation_rate=0.25,
    num_inputs=30)
population.sort(reverse=True)
print(population[0])

# Meta-heuristic specific components
from inspyred import ec
from random import Random
rand = Random()
ga = ec.GA(rand)
population: [ec.Individual] = ga.evolve(
    generator=generate,
    evaluator=evaluate,
    selector=ec.selectors.tournament_selection,  # optional
    tournament_size=10,
    pop_size=1000,
    maximize=False,
    bounder=ec.Bounder(0, 1),
    max_evaluations=20000,
    mutation_rate=0.01,
    num_cities=5,
    distance_matrix=distance_matrix)
population.sort(reverse=True)
print(population[0])

"""
 < <  E N D   O F   D O C U M E N T  > > 
"""

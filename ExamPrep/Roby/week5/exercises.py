from knapsack import knapsack_dp
from p_five import *

""" EXERCISES """
def obj_func(solution):
    s = len(solution) * 10
    for i in range(0, len(solution)):
        s = s + (solution[i] ** 2 - 10 * math.cos(2 * math.pi * solution[i]))
    return s


def generate(random=None, args=None) -> []:
    size = args.get('num_inputs', 10)
    return np.random.uniform(low=-5.12, high=5.12, size=size)


def evaluate(candidates, args={}):
    fitness = []
    for candidate in candidates:
        fitness.append(obj_func(candidate))
    return fitness


items = pd.read_csv('C:\\Users\ihsan\Desktop\Data & AI 2.1\CSVs\Knapsack_Items.csv', delimiter=',')
"-1-"

knapsack_dp(items['value'], items['weight(gr)'], len(items['value']), 750, return_all=True)
# ([0, 2, 4, 6, 7, 8, 13, 14], 1458.0)

"-2-"

"-3-"

"-4-"

"-5-"
'''Do above-average chromosomes always have better offspring?'''
# No, some combination of their genes may produce not so healthy(good) offsprings.
'''b.Can you explore the entire search space with crossover? Are there any limitations to this exploration?'''
# Yes. No.
'''c.Why does the risk of mutation play an important role?'''
# Mutations produce unintended gene combinations that can even
# lead to unknown disease or disables offspring.
# Due to reasons stated above, we want to avoid mutations as much as possible.
'''d.Why should Simulated Annealing start from a randomized vector?'''
# Simulated Annealing is a stochastic global search optimization algorithm.
# This means that it makes use of randomness as part of the search process.
# This makes the algorithm appropriate for nonlinear objective functions where
# other local search algorithms do not operate well.
'''e.Why should the temperature decrease with simulated annealing?'''
# The temperature corresponds to a control parameter temperature T in SA.
# T controls the probability of accepting a new solution that is worse than
# the current solution. If T is high, the acceptance probability is also high,
# and vice versa. T starts at the peak temperature, making the current solution
# changes almost randomly at first. T then gradually decreases, so that more and more
# suboptimal perturbations are rejected. The algorithm normally terminates when
# T reaches a user-specified value.


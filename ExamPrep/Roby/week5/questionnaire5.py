import math
import statistics

import numpy as np
import pandas as pd
import tsp
from python_tsp.exact import solve_tsp_dynamic_programming
from simanneal import Annealer

""" <<<  QUESTIONNAIRE - WEEK-5  >>> """
students = \
    pd.read_csv('D:\Intellij-IDEA\Python Projects\DATA & AI CLASSES\Exam Prep-2\csv_files\Questionnaire20-21.csv',
                sep=";", decimal=",")

"-1-"
def kendall_median(rv, rm):
    result = []
    for col in rm:
        corr_coef = rm[col].corr(rv, method='kendall')
        result.append(corr_coef)
    return statistics.median(result)


"-2-"
def split_choices(row):
    return str.split(row, ',')


split_choices(students['Favorite Fruit'][0])  # test the function on the first entry
choices_matrix = students['Favorite Fruit'].apply(split_choices)
choices_matrix.head()


def split_preferences(row):
    return np.array([str.split(el, '=') for el in row])


split_preferences(choices_matrix[0])  # test the function on the first entry
preferences_matrix = choices_matrix.apply(split_preferences)
preferences_matrix.head()


def sort_preferences(row):
    try:
        ranknumbers = row[:, 0].astype(int)  # first colomn contains the ranknumbers
        return row[ranknumbers.argsort()][:, 1]  # second colomn contains the types of fruit
    except Exception:  # eg. when no numbers used
        return np.repeat(np.nan, 10)


sort_preferences(preferences_matrix[0])  # test the function on the first entry
preferences_t1 = '11=Kiwi,6=Pineapple,7=Cherry,8=Banana,9=Strawberry,10=Apple,1=Melon,2=Pear,3=Plum,4=Orange'
sort_preferences(split_preferences(split_choices(preferences_t1)))
preferences_t2 = 'a=Kiwi,6=Pineapple,7=Cherry,8=Banana,9=Strawberry,10=Apple,1=Melon,2=Pear,3=Plum,4=Orange'
sort_preferences(split_preferences(split_choices(preferences_t2)))
fruit_series = preferences_matrix.apply(sort_preferences)  # only if no anomalies in input
fruit_series.head()

fruit_preferences = pd.DataFrame(zip(*fruit_series), index=range(1, 11)).transpose()



from inspyred import ec
from random import Random

def generate(random = None, args = None) -> []:
    pref_mat = args.get('pref_matrix',[])
    size = pref_mat.shape[0]
    sol = np.random.permutation(np.arange(1,size+1))
    solution = pd.Series(data=sol, name = 'a solution', index=pref_mat.index)
    return solution.tolist()

def evaluate(candidates, args = {}):
    pref_mat = args.get('pref_matrix',[])
    fitness = []
    for candidate in candidates:
        cand = pd.Series(data=candidate,index=pref_mat.index)
    result=[]
    for column in pref_mat:
        corr_coef = pref_mat[column].corr(cand, method = 'kendall')
    result.append(corr_coef)
    fitness.append(statistics.median(result))
    return fitness


rand = Random()
ga = ec.GA(rand)
population: [ec.Individual] = ga.evolve(
    generator=generate,
    evaluator=evaluate,
    selector=ec.selectors.tournament_selection,
    tournament_size=10,
    pop_size=100,
    maximize=True,
    bounder=ec.Bounder(1, fruit_preferences.shape[0]),
    max_evaluations=20000,
    mutation_rate=0.01,
    pref_matrix=fruit_preferences)
population.sort(reverse=True)
print(population[0])
# [15, 14, 21, 4, 1, 2, 3, 7, 11, 12, 18, 13, 9, 17, 19, 20, 6, 10, 8, 5, 16] : -0.11253233734905374

opt_sol = [5, 1, 4, 9, 7, 2, 8, 6, 10, 3]
opt_sol = pd.Series(data=opt_sol, name='Optimal solution', index=fruit_preferences)
kendall_median(opt_sol, fruit_preferences)

"-3-"


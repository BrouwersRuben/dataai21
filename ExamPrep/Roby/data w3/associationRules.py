## Association rules
from scipy.special import comb
from mlxtend.preprocessing import TransactionEncoder
import pandas as pd
import numpy as np
from mlxtend.frequent_patterns import apriori
from matplotlib import pyplot as plt
from mlxtend.frequent_patterns import association_rules

adultUCI = pd.read_csv('AdultUCI.csv', delimiter=';').dropna()

#c. Remove the following columns from the dataframe: fnlwgt, education-num, capitalgain, capitalloss.
adultUCIDroppedColumns = adultUCI.drop(columns=["fnlwgt", "education-num","capital-gain", "capital-loss"], axis=1)

#d. You can't work with numeric data. Therefore, we will convert the numeric columns to categories:
adultUCIDroppedColumns['age'] = pd.cut(adultUCIDroppedColumns['age'], [15, 25, 45, 65, 100], right=False, labels=['Young','Middle-aged','Senior','Old'])
adultUCIDroppedColumns['hours-per-week'] = pd.cut(adultUCIDroppedColumns['hours-per-week'], [0,25,40,60,168], right=False, labels=['Part-time','Full-time','Over-time','Workaholic'])

#print(adultUCIDroppedColumns['age'])
#print(adultUCIDroppedColumns['hours-per-week'])
#print(adultUCIDroppedColumns.info())

#e. Convert the dataframe to a transactional format with Pandas get_dummiesfunction. Also use the parameter prefix_sep='='. Take a look at the result.
adultUCItransactopnal = pd.get_dummies(adultUCIDroppedColumns, prefix_sep='=')
#print(adultUCItransactopnal)

#f.Create a bar chart of all items with a support of 0.1 or more
#print(itemsets['itemsets'].to_numpy())
def showBarChart():
    #Sorted by highest support first
    itemsets = apriori(adultUCItransactopnal, min_support=0.1, use_colnames=True).sort_values(by=['support'], ascending=False)
    #print(itemsets)
    #Convert to string or else error
    itemsets['itemsets'] = itemsets['itemsets'].astype('|S')
    fig = plt.figure()
    ax = fig.add_axes([0,0,1,1])
    ax.bar(itemsets['itemsets'], itemsets['support'])
    plt.show()

#showBarChart()

#g.Which two items have very high support? Can you conclude from this that the questionnaire taken is a good example of a random sample?
#A lot of people are natively from the US and are white.

#h.Apply the apriori and association_rules algorithms with the following parameters:➢support = 0.05,➢confidence = 0.6,➢minlen=2 and maxlen=3You can use the following filter function in combination with the .apply function of a DataFrame:
def associationRulesApriori(data, min_support, min_threshold, minlen, maxlen):
    #e. Convert the dataframe to a transactional format with Pandas get_dummiesfunction. Also use the parameter prefix_sep='='. Take a look at the result.
    #adultUCItransactopnal = pd.get_dummies(adultUCIDroppedColumns, prefix_sep='=')
    def rule_filter(row, minlen, maxlen):
        length = len(row['antecedents']) + len(row['consequents'])
        return minlen <= length <= maxlen

    itemsets = apriori(data, min_support=min_support, use_colnames=True)

    rules = association_rules(itemsets, metric='confidence', min_threshold=min_threshold)

    return rules[rules.apply(lambda row: rule_filter(row, minlen, maxlen), axis=1)].sort_values(by='confidence', ascending=False)

result = associationRulesApriori(adultUCItransactopnal, 0.05, 0.6, 2, 3)

#print(len(result))

#i.Check out the rules with the highest confidence? What stands out?
#print(result)
#Husband and male reoccurs a lot

#j. Can you explain why there is such a high confidence in this case?
#In itself, it is logical that the confidence is 1. All Husbands will indeed indicate that they are Male.

#k.
adultUCIDroppedColumns = adultUCIDroppedColumns.drop(columns=["relationship"], axis=1)

#L
adultUCItransactopnal = pd.get_dummies(adultUCIDroppedColumns, prefix_sep='=')
result = associationRulesApriori(adultUCItransactopnal, 0.05, 0.6, 2, 3)
print(result.iloc[0]['antecedents'])
print(result.iloc[0]['consequents'])

#m.If you look at the lift of this rule, would you still consider it as a good association rule? Why?
print(result.iloc[0]['lift'])
#Yes. The lift(1.476336) is well above 1. This means that there is more chance that someone has entered sex=Male if he has also entered marital-status=Married-civ-spouse,occupation=Craft-repair than that they have entered sex=Male in the full dataset

#n.If a respodent fills in that he is working overtime (hours-per-week) and has a limited income in which age category can we expect him to be in? How sure are you of that?
#Not finished: doesn't give the age
filtered = result.loc[result['antecedents'] == {'income=small', 'hours-per-week=Over-time'}]
#result.to_csv('filter.csv', index=False)
print(filtered['consequents'])
#pd.set_option("display.max_rows", None, "display.max_columns", None)
#print(result)
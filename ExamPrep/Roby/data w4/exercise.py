""""
Exercise1:Look for the dataset bordeaux on Canvas and place it in a dataframe.
a.Apply a discriminant analysis to this data set where quality acts as a dependent variable and temperature, sun, heat and rain as the independent variables.
b.How many dimensions does the discriminant analysis have?
c.Make a plot of the results of the discriminant analysis. What is being visualized here?
d.Apply the predict command to the original data. How many percent of the wines get the same label assigned through the discriminant analysis as observed?
"""

import numpy as np
import pandas as pd
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
import matplotlib.pyplot as plt
from sklearn.metrics import accuracy_score

bordeaux = pd.read_csv('bordeaux.csv', delimiter=';')

#a. Apply a discriminant analysis to this data set where quality acts as a dependent variable and temperature, sun, heat and rain as the independent variables.
x = bordeaux[['temperature', 'sun', 'heat', 'rain']]
y = bordeaux['quality']
lda = LinearDiscriminantAnalysis()
lda.fit(x,y)

"""
If you want  to analyze the type of data:
df.describe()
print(bordeaux.describe())
"""

"""
b. How many dimensions does the discriminant analysis have?
2: The dependant variable has 3 possible values and there are 4 independent variables
(3-1,4) = 2
print(len(lda.classes_))
"""
print("Answer to b")

def calculateDimensions(numbPossibleValues, numbIndValues):
    return print(min(numbPossibleValues-1, numbIndValues))

print(calculateDimensions(len(lda.classes_), len(x)))

#c. Make a plot of the results of the discriminant analysis. What is being visualized here?
LD = lda.transform(x)

LD_df = pd.DataFrame(zip(LD[:,0], bordeaux['quality']),columns=['LD1','Target'])

print(LD_df)

LD = lda.transform(x)

LD_df = pd.DataFrame(zip(LD[:, 0], bordeaux['quality']), columns=['LD1', 'Target'])
LD_df2 = pd.DataFrame(zip(LD[:, 1], bordeaux['quality']), columns=['LD1', 'Target'])

from matplotlib import pyplot as plt

colorPalette = LD_df
colorPalette['Target'].loc[(colorPalette['Target'] == 'good')] = 'green'
colorPalette['Target'].loc[(colorPalette['Target'] == 'medium')] = 'orange'
colorPalette['Target'].loc[(colorPalette['Target'] == 'bad')] = 'red'


def plot_step_lda():
    ax = plt.subplot(111)
    dfRowSize = len(LD)
    for _ in range(dfRowSize):
        plt.scatter(x=LD[:, 0],
                    y=LD[:, 1],
                    marker='o',
                    color=colorPalette.iloc[:, 1],
                    alpha=0.5,
                    )

    plt.xlabel('LD1')
    plt.ylabel('LD2')

    leg = plt.legend(loc='upper right', fancybox=True)
    leg.get_frame().set_alpha(0.5)
    plt.title('LDA: Results of the discriminant analysis')

    # hide axis ticks
    plt.tick_params(axis="both", which="both", bottom="off", top="off",
                    labelbottom="on", left="off", right="off", labelleft="on")

    # remove axis spines
    ax.spines["top"].set_visible(False)
    ax.spines["right"].set_visible(False)
    ax.spines["bottom"].set_visible(False)
    ax.spines["left"].set_visible(False)

    plt.grid()
    plt.tight_layout()
    plt.show()


plot_step_lda()
#d.Apply the predict command to the original data. How many percent of the wines get the same label assigned through the discriminant analysis as observed?
#print(lda.predict(x))
def printAccuracyLDA(original, predicted):
    #Original and predicted are still the same size of the original dataframe
    return(accuracy_score(original, predicted, normalize=False)/len(original))

print(printAccuracyLDA(y, lda.predict(x)))

"""
Exercise3:  Look for the dataset Cars93on Canvas and place it in a dataframe.
a)Analyze the type of data in this dataset.
b)Split the data set into two: the first 90 rows as a ‘trainingdata set' why you are going to apply a discriminant analysis (see e) and the last 3 rows to check whether the obtained discriminant analysis places these cars in the correct class (see g).
c)Whichcolumns can be used as a dependent variable for a discriminant analysis?
d)Which columns can be used as independent variables for a discriminant analysis?
e)Apply a discriminate analysis to this data set. As a dependent variable, take the Type column and as independent variables all usable columns (see d).
f)How many dimensions does the discriminant analysis have?
g)Apply the obtained discriminat analysis using predict command to the last 3 rows of the original data (see b). Are all 3 cars placed in the correct class?
"""

#fillna used to get the same answers
cars = pd.read_csv('Cars93.csv', delimiter=';', decimal='.').fillna(0)
print(len(cars))

#a. Analyze the type of data in this dataset.
#cars.describe()

#b. Split the data set into two: the first 90 rows as a ‘trainingdata set' why you are going to apply a discriminant analysis (see e) and the last 3 rows to check whether the obtained discriminant analysis places these cars in the correct class (see g).
cars1 = cars.iloc[:90,:]
cars2 = cars.iloc[90:,:]
#print(cars2)

#print(cars1.tail)
#print(cars2)

#c. Which columns can be used as a dependent variable for a discriminant analysis?
#any of the qualitative variables: Manufacturer, Model, Type, Airbags, DriveTrain, Man.trans.avail, origin, makeand also Cylinders.

#d. Which columns can be used as independent variables for a discriminant analysis?
#all of thequantitative variables:  Min.Price, Price, Max.Price, MPG.city, MPG.highway, EngineSize, Horsepower, RPM, Rev.per.mile, Fuel.tank.capacity, Passengers, Length, Wheelbase, Width, Turn.circle, Rear.seat.room, Luggage.room, Weight

#e. Apply a discriminate analysis to this data set. As a dependent variable, take the Type column and as independent variables all usable columns (see d).
x = cars1[['Min.Price', 'Price', 'Max.Price', 'MPG.city', 'MPG.highway', 'EngineSize', 'Horsepower', 'RPM', 'Rev.per.mile', 'Fuel.tank.capacity', 'Passengers', 'Length', 'Wheelbase', 'Width', 'Turn.circle', 'Rear.seat.room', 'Luggage.room', 'Weight']]
y = cars1['Type']

lda = LinearDiscriminantAnalysis()
lda.fit(x,y)

#f. How many dimensions does the discriminant analysis have?
# zijn de total possible values
# dus dit zijn de possible values van Type(de dependent variable die we gebruiken)
# ['Small' 'Midsize' 'Compact' 'Large' 'Sporty' 'Van']
# aka 6
# en 18 is het totaal van independent variables
# exclusief de extra dependent variables, die laten we er gewoon uit want die hebben geen nut
print('Total possible values: ' + str(len(cars1['Type'].unique())))
print("F: ")
print(calculateDimensions(6, 18))
print(calculateDimensions(cars['Type'].nunique(), 18))

#g. Apply the obtained discriminat analysis using predict command to the last 3 rows of the original data (see b). Are all 3 cars placed in the correct class?
testPrediction = cars2[['Min.Price', 'Price', 'Max.Price', 'MPG.city', 'MPG.highway', 'EngineSize', 'Horsepower', 'RPM', 'Rev.per.mile', 'Fuel.tank.capacity', 'Passengers', 'Length', 'Wheelbase', 'Width', 'Turn.circle', 'Rear.seat.room', 'Luggage.room', 'Weight']]
print(printAccuracyLDA(cars2['Type'], lda.predict(testPrediction)))
print(lda.predict(testPrediction))

import pandas as pd
import math
from scipy.stats import chi2
from scipy.stats import t
from scipy.stats import norm
from scipy.stats import chisquare
import numpy as np

"""
What different tests are there: 
    - chi-square
        When the variables are independant from each other and belong to thesame population
        When the standard deviation is not given
    - t-test
        Sample size < 30
        Standard deviation is given
    - z-test
        Sample size > 30
        Standard deviation is given
        
    Distributions:
    - normal distribution
        When the std is known
    - student distribution
        When the std is not known
"""

"""
Excercise 1
Open the file "gamerps.csv". Rock, paper, scissors is a game where you make a choice
between these three. However, is there a preference among our scout members of the
selection test? The expected distribution is proportional. Check if our count differs from the
expected distribution
a. Which test will you use for this?
b. Formulate H0 (“The count in our sample [is/is not] evenly distributed”).
c. What is the value of ²? (3 significant figures)
d. What is the probability that a sample would have a value higher than ² (p-value)? (3
significant figures)
e. Can H0 be rejected with a 95% reliability?
f. So can we say that the scout members have a preference?
"""

gamerps = pd.read_csv('gamerps.csv')
print(len(gamerps.loc[gamerps['hand'] == 'Paper']))
#a. chi square - no std given

#b. Formulate H0 (“The count in our sample [is/is not] evenly distributed”).
# h0 = The count in our sample is evenly distributed
# h1 = The count in our sample is not evenly distributed

#c. What is the value of ²? (3 significant figures)
#(recordedAmount/expectedAmount)²/expectedAmount + ...--> repeat for every signficant figure
# --> (30-25)²/25 + (21-25)²/25 + (24-25)²/25
x2 = (30-25)**2/25 + (21-25)**2/25 + (24-25)**2/25
print(x2)

#d. What is the probability that a sample would have a value higher than ² (p-value)? (3 significant figures)
print(1-chi2.cdf(x2, 3-1))
# chi2.cdf(², m-1)
# m is the number of values counted(4 in our case)
# 3 voor die oefening

#e. Can H0 be rejected with a 95% reliability?
#Cannot reject H0, p-value is higher than alpha (0.4317 > 0.05)

#f. So can we say that the scout members have a preference?
#Based on the results, we can say that the scout member don't have a preference

"""
Excercise 3
We want to know how many requests a server has to process on average per day. We do a
measurement for that. We measure for 30 days and count the number of requests every day.
We arrive at an average of 975 and a standard deviation equal to 100.
a. Between what limits is the expected number of requests per day if we want to be 95%
sure?
b. Suppose we found the same mean and standard deviation, but with a sample of 100
days. Then we are 95% sure that the expected number of requests per day is
between ... and....
c. Suppose someone claims that the server needs to process 1000 requests per day. Can
you then support or reject this statement in the two cases? You want a 95% certainty
again
"""

#a. Between what limits is the expected number of requests per day if we want to be 95% sure?
#1 - alpha = % chance
alpha = 0.05
#amount of occurences
n = 30
#mean
x_bar = 975
#standard deviation
s = 100

print('a. 30 days')
print(t.interval(alpha=0.95, df=n-1, loc=x_bar, scale=s/math.sqrt(n)))

#b. Suppose we found the same mean and standard deviation, but with a sample of 100 ...
print('b. 100 days')
n = 100
print(t.interval(alpha=0.95, df=n-1, loc=975, scale=s/math.sqrt(n)))

#c. Suppose someone claims that the server needs to process 1000 requests per day. Can you then support or reject this statement in the two cases? You want a 95% certainty again
print('c')
#In solution but is this even necessary?
x_bar = 1000
#for case a
print('c, case a: 30 days, 1000 requests')
n = 30
print(t.interval(alpha=0.95, df=n-1, loc=x_bar, scale=s/math.sqrt(n)))

#for case b
print('c, case b: 100 days, 1000 requests')
n = 100
print(t.interval(alpha=0.95, df=n-1, loc=x_bar, scale=s/math.sqrt(n)))

"""
a. 30 days
(937.65938632419, 1012.34061367581)
b. 100 days
(955.1578304849131, 994.8421695150869)

for case a: can't be rejected
for case b: reject
"""

"""
Excercise 5
Commissioned by a cheese factory, we investigate whether some suppliers tamper with their
milk by adding water to it.
We take 5 consecutive shipments of milk and see at what temperature it freezes.
We know that the freezing point of milk is equal to -0.545°C with a standard deviation of
0.008°C. The freezing point of water is of course 0°C.
In our sample we find an average freezing point of -0.539 °C. Use alpha = 0.1
a. Which statistical test are we going to use?
b. Should we apply this test unilaterally or two-sidedly?
c. Has the milk been tampered with?
d. What is the probability that the previous answer is wrong?

"""

#a. Which statistical test are we going to use? 
#z-test --> std is known
#And normal distribution since std is known

#b. Should we apply this test unilaterally or two-sidedly?
#Two-sidedly because there we're testing if it's tampered with (can be with water or another substance). If we want to test if it's only tampered with water we use unilaterally.

#c. Has the milk been tampered with?
print('exercise 5: c')
alpha = 0.9
x_bar = -0.545
std = 0.008
n = 5
print(norm.interval(alpha=alpha, loc=x_bar, scale=std/math.sqrt(n)))
#(-0.5508848072366409, -0.5391151927633592)
#With our tests, we cannot say that the milk's been tampered with (-0.539 just falls inside of our acceptance region)

#d. What is the probability that the previous answer is wrong?
#10% --> look at alpha

"""
Excercise 6
We want to hire a programmer. We subject the candidates to a test. We know that a good
programmer gets an average score of 100 on this test.
Some (16) students of KdG particate. We arrive at an average of 107.3 and a standard
deviation of 8.0.
Take alpha=0.05. We suspect that this group differs from the average population.
a. Which test do we use?
b. What factor are we going to use?
c. We can set up an interval within which we are 95% sure that KdG students score on
average.
d. Based on this interval, can we say that KdG students score better than average?
"""
print('exercise 6')
n = 16
µ = 100
x_bar = 107.3
s = 8
alpha = 0.95

#a. Which test do we use?
#16 and std known --> small sample size, use t-test

#b. What factor are we going to use?
# You can calculate the factor in Python with: 
# norm.ppf((1+p)/2) 
# or :
# norm.ppf(1 -/2)

factor = t.ppf((1+0.95)/2, n-1)
print(factor)

#c. We can set up an interval within which we are 95% sure that KdG students score on average.
print(t.interval(alpha=alpha, df=n-1, loc=x_bar, scale=s/math.sqrt(n)))

#d. Based on this interval, can we say that KdG students score better than average?
#Yes, the minimum in the interval is 100

"""
Excercise 8
A programmer has written a Python class to generate random integers between 0 and 10. The
code is as follows:
import math
class Random:
 def __init__(self)-> None:
 self.i = 0
 self.j = 0
 def random_between_0_and_10(self) -> int:
 r = abs(math.sin(51 * self.i ** 2 + math.cos(80 *
self.j)) + math.sin(300 * self.j + 3 * math.sin(111 * self.i -
self.j)))
 self.j += 1
 if self.j > 100:
 self.i +=1
 if self.i > 100:
 self.i = 0
 self.j = 0
 return math.floor(r * 5.5)
Write some Python code that uses this program and that generates 1100 random numbers.
Check if this random number generator works properly (we expect that every number 0-10
occurs equally). Use alpha=0.05.
a. What ²-value do you find?
b. What p-value do you find?
c. What is the confidence interval for alpha=0.05?
d. What decision can you draw?
"""

class Random:
    def __init__(self)-> None:
        self.i = 0
        self.j = 0
    def random_between_0_and_10(self) -> int:
        r = abs(math.sin(51 * self.i ** 2 + math.cos(80 * self.j)) + math.sin(300 * self.j + 3 * math.sin(111 * self.i - self.j)))
        self.j += 1
        if self.j > 100:
            self.i +=1
            if self.i > 100:
                self.i = 0
            self.j = 0
        return math.floor(r * 5.5)

random = Random() 
numbers = pd.Series([random.random_between_0_and_10() for i in range(1100)])
#print(numbers)

#a. What x²-value do you find?
# (recordedAmount/expectedAmount)²/expectedAmount - -> do this for every significant figure

"""
def calcx2():
    amount = 0
    for i in range(0,11):
        print('Calculating occurences of' + str(i))
        amount += (numbers.values == i).sum()
"""

def calcx2():
    x2 = 0
    expectedAmount = 1100 / 11
    for i in range(0,11):
        #print('Calculating occurences of ' + str(i))
        print(str(i) + ' has been found ' + str((numbers.values == i).sum()) + ' times')
        amount = (numbers.values == i).sum()
        x2 += ((amount/expectedAmount)**2/expectedAmount)

    return x2


x2 = calcx2()

print("x2 value of exercise 8:")
print(x2)

#b. What p-value do you find?
print("p-value of exercise 8:")
print(1-chi2.cdf(x2, 11-1))

#c. What is the confidence interval for alpha=0.05?
#idk :(

#d. What decision can you draw?
#The random number generator doesn't work properly --> really low x2 value (x2 > 1 = good, in this case it's 0.1246)

import numpy as np
import pandas as pd
import math
import matplotlib.pyplot as plt
from scipy.stats import norm

## Confidence interval –”Central limit theorem” simulation

def createPopulation (min, max, precision, spread, number, type):
    factor=1.0
    for i in range (0, precision):
        factor = factor * 10.0
    pop = np.empty(number,dtype=np.float64)
    if (type == 0): # uniform distribution
        for i in range (0,number):
            value = np.random.random() * (max-min) + min
            pop[i] = math.trunc(value * factor)/factor
    else: # normal distribution
        for i in range (0,number):
            while True:
                value = np.random.normal()
                value = value * spread/6 + (min+max)/2
                if min <= value <= max: break
            pop[i] = math.trunc(value * factor)/factor
    return pop


def createSample(pop,size):
    sample = np.empty(size,dtype=np.float64)
    pop_size = len(pop)
    for i in range (0,size):
        index = np.random.randint(0,pop_size)
        sample[i]=pop[index]
    return sample
    

def simulationSamples (pop, number, sample_size):
    sim = np.empty(number,dtype=np.float64)
    for i in range (0,number):
        sim[i]=createSample(pop,sample_size).mean()
    return sim
        
population = createPopulation (145,210,1,5,15000,1)
print(population.mean())
pd.Series(population).plot.hist(title='Distribution of the Population')
plt.show()

sample = createSample(population,15)
print(sample.mean())

simulations = simulationSamples (population,1000,15)
print(simulations.mean())

pd.Series(simulations).plot.hist(title='Distribution of the means obtained by samples')
plt.show()

# Confidence interval

df_c = pd.read_csv('consumptionLaptops.csv', decimal='.')

x_bar = df_c.consumptionLaptops.mean()
s = df_c.consumptionLaptops.std()
n= len(df_c)

factor = norm.ppf((1+0.955)/2)
interval = (x_bar-factor*s/math.sqrt(n), x_bar+factor*s/math.sqrt(n))

#OR
 
norm.interval(alpha=0.955, loc=x_bar, scale=s/math.sqrt(n) )

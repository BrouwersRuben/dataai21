# Student Distribution

from scipy.stats import t 
import math

t.cdf(x,df, loc=μ, scale=s) #s as approx. of 
t.ppf(q,df, loc=μ, scale=s) #s as approx. of 

df_c = pd.read_csv('consumptionLaptops.csv', decimal='.')

x_bar = df_c.consumptionLaptops.mean()
s = df_c.consumptionLaptops.std()
n= len(df_c)

factor = t.ppf((1+0.955)/2, n-1, loc= x_bar, scale=s/math.sqrt(n))

interval = (x_bar-factor*s/math.sqrt(n), x_bar+factor*s/math.sqrt(n))

#OR

t.interval(alpha=0.955, df=n-1, loc=x_bar, scale=s/math.sqrt(n))

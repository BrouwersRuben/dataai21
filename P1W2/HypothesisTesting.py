# Hopythesis Testing

import pandas as pd
import numpy as np
import math
from scipy.stats import binom
from scipy.stats import norm
from scipy.stats import t

df_c = pd.read_csv('consumptionLaptops.csv', decimal='.')

x_bar = df_c.consumptionLaptops.mean()
s = df_c.consumptionLaptops.std()
n= len(df_c) 
mu0=31

factor = t.ppf((1+0.95)/2, n-1)
interval = (mu0-factor*s/math.sqrt(n), mu0+factor*s/math.sqrt(n))

#OR

t.interval(alpha=0.95, df=n-1, loc=mu0, scale=s/math.sqrt(n))
# t.interval(alpha=0.95, df=30-1, loc=40,scale=20/math.sqrt(30))

# one sample t-test
from scipy.stats import ttest_1samp
screens= pd.read_csv('screens.csv',delimiter=';', decimal='.')

screens["New_size"].mean()
screens["New_size"].std()
mu=40

ttest_1samp(screens["New_size"],mu)

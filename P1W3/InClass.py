import pandas as pd

## Evaluation metrics
# Binary and multi-class

# This is all per class
def accuracy(TP, TN, FP, FN):
    accuracy = (TP + TN) / (TP + FP + TN + FN)
    return accuracy

# Note: other name ==> Positive Predictive Value (PPV)
def precision(TP, FP):
    precision = (TP) / (TP + FP)
    return precision

def recall(TP, FN):
    recall = TP / (TP + FN)
    return recall

# F-Measure
# TODO: Make this function

# Metrics only for binairyclassification
def TruePositiveRate(TP, FN):
    TPR = TP / (TP+FN)
    return TPR;

def FalsePositiveRate(FP, TN):
    FPR = FP / (FP+TN)
    return FPR;

# AUC = Area under curve
# 1 = very good, 0.5 = very bad

# Determine how good our classification is 
# TPR ^ (good) --> FPR ^ (poor)
# FPR ↓ (good) --> TPR ↓ (poor)

## Association rules
receipts = pd.read_csv('transactions.csv', sep=';', index_col=0)
transactionstable = pd.get_dummies(receipts, columns=['Product'],prefix='',prefix_sep='').groupby(level='TransactionID').sum()

from mlxtend.preprocessing import TransactionEncoder
transactions = [['Printer', 'Cartridge', 'Ballpoint'], ['Paper', 'Cartridge'],['Paper', 'Cartridge'], ['Printer', 'Cartridge'],['Printer', 'Cartridge'],  ['Ballpoint'], ['Paper', 'Cartridge', 'Ballpoint'],['Cartridge'], ['Printer', 'Ballpoint'],['Paper']]
transactionID=[1000123, 1000124, 1000125, 1000126, 1000127, 1000128, 1000129, 1000230, 10001311000132]
te = TransactionEncoder()
dataset = te.fit(transactions).transform(transactions)
transactionstable = pd.DataFrame(dataset,columns=te.columns_, index= transactionID)

from scipy.special import comb
print(comb(4, 1))
print(comb(4, 2))
print(comb(4, 3))
print(comb(4, 4))
print(sum([comb(4,i) for i in range(1, 5)]))

print(sum([comb(1000,i) for i in range(1,1001)]))









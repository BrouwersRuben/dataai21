import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

# Step 1: Make sure you have your data available:
# download the file iris.csv from canvas. 
iris = pd.read_csv('iris.csv',delimiter=',',decimal='.')
iris.info()

# Step 2: Make a model that you can use to predict
# Linear Discriminant Analysis will be covered later. All you need
# to know for now is that you can predict "Species" based on the
# Sepal and Petal columns.
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
X = iris[['sepal length', 'sepal width', 'petal length', 'petal width']]
y = iris['target']
model = LinearDiscriminantAnalysis()
model.fit(X, y)

# Step 3: Create a confusion matrix
# Once you have created a model, you can predict the classes with
# the predict function 
real = iris.target
listoflabels =real.unique().tolist()
predicted = model.predict(X) 
# Create a confusion array with actual values and predicted values.
from sklearn.metrics import confusion_matrix
conf_matrix = confusion_matrix(real,predicted,labels = listoflabels)
# Confusion matrix whose i-th row and j-th column entry indicates
# the number of samples with true label being i-th class and
# predicted label being j-th class.
conf_matrix
# visualisation of the confusion matrix
fig, ax = plt.subplots(figsize=(5, 5))
ax.matshow(conf_matrix, cmap=plt.cm.Oranges, alpha=0.3)
for i in range(conf_matrix.shape[0]):
    for j in range(conf_matrix.shape[1]):
        ax.text(x=j, y=i,s=conf_matrix[i, j], va='center', ha='center', size='xx-large')
plt.xlabel('Predictions', fontsize=18)
plt.ylabel('Actuals', fontsize=18)
ax.set_xticks(range(0,len(listoflabels)))
ax.set_xticklabels(listoflabels)
ax.set_yticks(range(0,len(listoflabels)))
ax.set_yticklabels(listoflabels)
plt.title('Confusion Matrix', fontsize=18)
plt.show()

# Step 4: Calculate the different metrics
# diagrepresents the values on the diagonal
from sklearn.metrics import accuracy_score, precision_recall_fscore_support
print(real.unique())
accuracy_score (y_true=real,y_pred=predicted)
precision_recall_fscore_support (y_true=real,y_pred=predicted,labels = real.unique(),beta=1.0)

